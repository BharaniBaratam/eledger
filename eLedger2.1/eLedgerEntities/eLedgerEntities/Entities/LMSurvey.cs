﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace eLedgerEntities
{
    [DataContract]
    public class LMSurvey
    {
        [DataMember]
        public List<LMSurveyDetail> LMSurveyDet { get; set; }
        [DataMember]
        public List<LMSurveyDetail1> LMSurveyDet1 { get; set; }
        [DataMember]
        public List<LMSurveyDetail2> LMSurveyDet2 { get; set; }
    }
    public class LMSurveyDetail
    {
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string distance { get; set; }
        [DataMember]
        public string LocId { get; set; }
        [DataMember]
        public string LocName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Zip { get; set; }
        [DataMember]
        public string WD { get; set; }
        [DataMember]
        public string SurveyedOn { get; set; }
       
    }
    public class LMSurveyDetail1
    {
        [DataMember]
        public string distance1 { get; set; }
        [DataMember]
        public string LocId1 { get; set; }
        [DataMember]
        public string LocName1 { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string City1 { get; set; }
        [DataMember]
        public string State1 { get; set; }
        [DataMember]
        public string Zip1 { get; set; }
        [DataMember]
        public string WD1 { get; set; }
        [DataMember]
        public string SurveyedOn1 { get; set; }
       
    }
    public class LMSurveyDetail2
    {
        [DataMember]
        public string distance2 { get; set; }
        [DataMember]
        public string LocId2 { get; set; }
        [DataMember]
        public string LocName2 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string City2 { get; set; }
        [DataMember]
        public string State2 { get; set; }
        [DataMember]
        public string Zip2 { get; set; }
        [DataMember]
        public string WD2 { get; set; }
        [DataMember]
        public string SurveyedOn2 { get; set; }
       
    }
}