﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eLedgerEntities
{
    public class MainModel
    {
        public Location _location = new Location();
        public List<Alert> _alerts = new List<Alert>();
        public Agreement _agreement = new Agreement();
        public Activity _activity = new Activity();
        public Alert _alert = new Alert();
        public List<Notes> _notes = new List<Notes>();
        public Notes _note = new Notes();
        public Equipment _equipment = new Equipment();
        public Revenue _revenue = new Revenue();
        public Service _service = new Service();
        public Legal _legal = new Legal();
        public Contact _contact = new Contact();
        public List<Contact> _contacts = new List<Contact>();
        public List<Location> _locations = new List<Location>();
        public List<Activity> _Activities = new List<Activity>();
        public List<Legal> _legalissues = new List<Legal>();
        public LMSurvey _LMSurvey = new LMSurvey();
        public Revenue_New _revenuenew = new Revenue_New();
        public Widget _Widget = new Widget();
        public string searchULN;
        public string searchContractType;
        public string SearchContactId;
        public List<Widget> widgetMenuList = new List<Widget>();
        public string widgets;
        public MainModel()
        {
            //_location = null;
            //_alerts = null;
            //_agreement = null;
            //_activity = null;
            //_alert = null;
            //_notes = null;
            //_note = null;
            //_equipment = null;
            //_revenue = null;
            //_service = null;

            //_legal = null;
            //_contact = null;
            //_locations = null;
            



        }

      
        
    }
}