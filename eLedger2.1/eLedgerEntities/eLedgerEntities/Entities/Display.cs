﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eLedgerEntities
{
    public class Display
    {
        public string Column_Name { get; set; }
        public string Column_Value { get; set; }
        //public string ColumnRte { get; set; }
        //public string ColumnRteDay { get; set; }
        public string ColumnSrvGrp_Value { get; set; }
        //public string ColumnRte_Value { get; set; }
        //public string ColumnRteDay_Value { get; set; }
        public List<Display> displaydata { get; set; }

    }
}