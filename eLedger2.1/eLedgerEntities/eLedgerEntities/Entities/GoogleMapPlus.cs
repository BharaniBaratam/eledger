﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eLedgerEntities
{
    public class GoogleMapPlus
    {        
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
    }
}