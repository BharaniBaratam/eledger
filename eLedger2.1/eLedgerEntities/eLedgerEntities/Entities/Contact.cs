﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace eLedgerEntities
{
    [DataContract]
    public class Contact 
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string ContactName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Zip { get; set; }
        [DataMember]
        public string Phone1 { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string ContactId{ get; set; }
        [DataMember]        
        public string Comments{ get; set; }
        [DataMember]
        public string UserName{ get; set; }
        [DataMember]
        public string CommentDate { get; set; }
        [DataMember]
        public string ContactTypeCode{ get; set; }
        [DataMember]
        public string CMS { get; set; }
        [DataMember]
        public string Name{ get; set; }
        [DataMember]
        public string ContactInfo{ get; set; }
        [DataMember]
        public string Address1{ get; set; }
        [DataMember]
        public string ActiveLocs { get; set; }
        [DataMember]
        public string Machines { get; set; }
        [DataMember]      
        public string Flag { get; set; }
        [DataMember] 
        public List<Display> displaydata { get; set; }
        [DataMember]
        public int rows { get; set; }
        [DataMember]
        public int page { get; set; }
        [DataMember]
        public string hasMCNotes { get; set; }
        [DataMember]
        public string noteHeight { get; set; }
        [DataMember]
        public string TaxId { get; set; }
        [DataMember]
        public string Ten99Type { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string Phone1_Fmt { get; set; }
        [DataMember]
        public string Phone2_Fmt { get; set; }
        [DataMember]
        public string FaxNum_Fmt { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string showmastercard { get; set; }
        [DataMember]
        public List<ContactMCN> contactMCN { get; set; }
        [DataMember]
        public List<ContactrelatedLoc> contactrelatedloc { get; set; }
    }
    [DataContract]
    public class ContactMCN
    {
        [DataMember]
        public string CommentDate { get; set; }
         [DataMember]
        public string Comments { get; set; }
         [DataMember]
         public string UserName { get; set; }
            
       
    }
    [DataContract]
    public class ContactrelatedLoc
    {
        public string RelatedType { get; set; }
        [DataMember]
        public string ULN { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public string PropertyAddress { get; set; }
        [DataMember]
        public string PropertyCity { get; set; }
        [DataMember]
        public string PropertyState { get; set; }
        [DataMember]
        public string PropertyZip { get; set; }
        [DataMember]
        public string PropertyStatus { get; set; }
        [DataMember]
        public string PropertyCotractType { get; set; }
       
    }

}

