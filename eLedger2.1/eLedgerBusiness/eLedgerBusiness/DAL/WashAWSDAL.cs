﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Collections;
using System.IO;
using System.Text;
using Npgsql;
using System.Collections.Specialized;
using System.Threading;

namespace eLedgerBusiness.DAL
{
    public class WashAWSDAL
    {
        string ConStr;

        public void initAWSConnectionString()
        {
            ConStr = System.Configuration.ConfigurationManager.AppSettings["AWS_Conn"];
        }
        public void CMSinitConnectionString()
        {
            ConStr = System.Configuration.ConfigurationManager.AppSettings["CMS"];
        }
        public void ASAPinitConnectionString()
        {
            ConStr = System.Configuration.ConfigurationManager.AppSettings["ASAP_SQL_Conn"];
        }

        public NpgsqlConnection openConnection1()
        {
            initAWSConnectionString();
            NpgsqlConnection dbConn = new NpgsqlConnection(ConStr);
            dbConn.Open();
            return dbConn;

        }

        public DataTable getDataTableRetry(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            DataTable dt = new WashAWSDAL().getDataTable(queryCode, paramNames, paramValues);
            int RetriesCount = 0;

            while (dt.Rows.Count == 0)
            {
                RetriesCount++;
                if (RetriesCount > 2)
                    break;
                else
                {
                    Thread.Sleep(1000);
                    dt = new WashAWSDAL().getDataTable(queryCode, paramNames, paramValues);
                }
            }


            return dt;
        }

        public DataTable getDataTable(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            DataTable dt = new DataTable();
            try
            {

                initAWSConnectionString();
                using (NpgsqlConnection dbConn = new NpgsqlConnection(ConStr))
                {
                    dbConn.Open();
                    string query = getAWSQuery(queryCode);
                    
                    using (NpgsqlCommand dbCommand1 = new NpgsqlCommand(query, dbConn))
                    {
                        dbCommand1.CommandTimeout = 300;
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand1.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }
                        using (NpgsqlDataAdapter dbDataAdapter = new NpgsqlDataAdapter(dbCommand1))
                        {
                            
                            dbDataAdapter.Fill(dt);

                        }

                        //NpgsqlDataReader dReader = dbCommand1.ExecuteReader();
                        //dt.Load(dReader);
                       
                    }


                    dbConn.Close();
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return dt;
        }

        public DataSet getdatasetfromColumnRetry(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns, int tableNumber)
        {
            DataSet ds = new WashAWSDAL().getdatasetfromColumn(queryCode, paramNames, paramValues, Columns);
            int RetriesCount = 0;

            while (ds.Tables[tableNumber].Rows.Count == 0)
            {

                RetriesCount++;

                if (RetriesCount > 2)
                    break;
                else
                {

                    Thread.Sleep(1000);
                    ds = new WashAWSDAL().getdatasetfromColumn(queryCode, paramNames, paramValues, Columns);
                }
            }

            return ds;
        }

        public DataSet getdatasetfromColumn(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            DataSet ds = new DataSet();
            try
            {
                using (NpgsqlConnection dbConnection = openConnection1())
                {
                    using (NpgsqlCommand dbCommand = new NpgsqlCommand())
                    {
                        dbCommand.Connection = dbConnection;
                        string query = getAWSQuery(queryCode);
                        query = query.Replace("@Columns", Columns);
                        dbCommand.CommandText = query;
                        dbCommand.CommandTimeout = 0;
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }
                        using (NpgsqlDataAdapter dbDataAdapter = new NpgsqlDataAdapter(dbCommand))
                        {
                            dbDataAdapter.Fill(ds);
                        }
                    }
                    dbConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //closeConnection();
            return ds;

        }

        public DataTable getDataTableColumnsRetry(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            DataTable dt = new WashAWSDAL().getDataTableColumns(queryCode, paramNames, paramValues, Columns);

            int RetriesCount = 0;

            while (dt.Rows.Count == 0)
            {
                RetriesCount++;
                if (RetriesCount > 2)
                    break;
                else
                {
                    Thread.Sleep(1000);
                    dt = new WashAWSDAL().getDataTableColumns(queryCode, paramNames, paramValues, Columns);
                }
            }

            return dt;
        }

        public DataTable getDataTableColumns(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            DataTable dt = new DataTable();
            try
            {
                //openConnection();               
                initAWSConnectionString();
                using (NpgsqlConnection dbConn = new NpgsqlConnection(ConStr))
                {
                    string query = getAWSQuery(queryCode);
                    query = query.Replace("@Columns", Columns);
                    using (NpgsqlCommand dbCommand1 = new NpgsqlCommand(query, dbConn))
                    {
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand1.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }

                        using (NpgsqlDataAdapter dbDataAdapter = new NpgsqlDataAdapter(dbCommand1))
                        {
                            dbDataAdapter.Fill(dt);
                        }
                    }
                    dbConn.Close();
                }

            }
            catch (Exception e)
            {
                throw e;
                //throw e;
            }
            return dt;
        }

        public DataSet getAgmtInfoDSRetry(string queryCode, ArrayList paramNames, ArrayList paramValues, int tableNumber)
        {
            DataSet ds = new WashAWSDAL().getAgmtInfoDS(queryCode, paramNames, paramValues);

            int RetriesCount = 0;

            while (ds.Tables[tableNumber].Rows.Count == 0)
            {
                RetriesCount++;
                if (RetriesCount > 2)
                    break;
                else
                {
                    Thread.Sleep(1000);
                    ds = new WashAWSDAL().getAgmtInfoDS(queryCode, paramNames, paramValues);
                }
            }

            return ds;
        }

        public DataSet getAgmtInfoDS(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            DataSet ds = new DataSet();
            try
            {
                using (NpgsqlConnection dbConnection = openConnection1())
                {
                    using (NpgsqlCommand dbCommand = new NpgsqlCommand())
                    {
                        dbCommand.Connection = dbConnection;
                        dbCommand.CommandText = getAWSQuery(queryCode);
                        dbCommand.CommandTimeout = 0;
                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }
                        using (NpgsqlDataAdapter dbDataAdapter = new NpgsqlDataAdapter(dbCommand))
                        {
                            dbDataAdapter.Fill(ds);

                        }
                    }
                    dbConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            //closeConnection();
            return ds;

        }


        public string getJsonFromQuery(string queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            string sb = "";


            DataTable dt = getDataTable(queryCode, paramNames, paramValues);
            if (queryCode == "Activity") {
                dt = new WashAWSDAL().RenameColumns(dt, "GetActivity");
            }
            if (queryCode == "serviceSummary")
            {
                 dt = new WashAWSDAL().RenameColumns(dt, "ServiceSummary");
            }

            try
            {
                sb = GetJson(dt);
            }
            catch (OdbcException odbcException)
            { // exception                    
                throw odbcException;
            }
            finally
            {
                // close the connection
            }

            return sb;
        }

        public string getJsonFromQueryColumns(string queryCode, ArrayList paramNames, ArrayList paramValues, string Columns)
        {
            int rowCount = 0;
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            initAWSConnectionString();
            using (NpgsqlConnection dbConn = new NpgsqlConnection(ConStr))
            {
                try
                {
                    DataTable dt = getDataTableColumns(queryCode, paramNames, paramValues, Columns);

                    if (queryCode == "getLocationsListJson" || queryCode == "getLocationsListJson2") {
                        dt = new WashAWSDAL().RenameColumns(dt, "GetLocationsListJson");
                    }
                    if (queryCode == "getAlertTypeList")
                    {
                        dt = new WashAWSDAL().RenameColumns(dt, "getAlertTypeList");
                    }
                    


                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;

                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            if (col.ColumnName == "ULN")
                                if (dr[col].ToString().Length > 8)
                                {
                                    row.Add(col.ColumnName, dr[col].ToString().Substring(0, 4) + "-" + dr[col].ToString().Substring(4, 2) + "-" + dr[col].ToString().Substring(6, 3));
                                }
                                else
                                {
                                    row.Add(col.ColumnName.Trim(), dr[col]);
                                }
                            else
                                row.Add(col.ColumnName.Trim(), dr[col]);
                        }
                        rows.Add(row);
                    }
                    string jsonOutput = serializer.Serialize(rows); ;
                    if (jsonOutput.ToString().Length > 0)
                        jsonOutput = jsonOutput + ",\"total\":\"" + dt.Rows.Count + "\"";
                    return jsonOutput;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    dbConn.Close(); // close the connection
                }
            }
        }

        public int executeQuery(String queryCode, ArrayList paramNames, ArrayList paramValues)
        {
            int j = 0;
            try
            {
                using (NpgsqlConnection dbConnection = openConnection1())
                {
                    using (NpgsqlCommand dbCommand = new NpgsqlCommand())
                    {
                        dbCommand.Connection = dbConnection;
                        dbCommand.CommandText = getAWSQuery(queryCode);

                        if (paramNames != null)
                        {
                            for (var i = 0; i < paramNames.Count; i++)
                            {
                                string paramName = paramNames[i].ToString();

                                if (paramValues[i] == null)
                                {
                                    paramValues[i] = String.Empty;
                                }
                                dbCommand.Parameters.AddWithValue(paramName, paramValues[i]);
                            }
                        }

                        j = dbCommand.ExecuteNonQuery();
                    }
                    dbConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                //closeConnection();
                //dbConnection.Close();
            }
            return j;

        }




        public String getAWSQuery(string queryCode)
        {
            return eLedgerBusiness.Properties.WashAWSQueries.ResourceManager.GetString(queryCode);
        }

        //Create the LOG File
        public void CreateLog(string exception, string method, string param)
        {

            // Create a writer and open the file:            
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            //string UserName = "WASHLAUNDRY\\BBARATAM";
            if (UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;


            var fpath = string.Empty;
            var path = string.Concat(AppDomain.CurrentDomain.BaseDirectory, @"\log\");
            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);
            fpath = path + "ErrorLog_" + DateTime.Now.ToString("dd-MMM-yy") + ".txt";
            if (System.IO.File.Exists(fpath) == false)
                System.IO.File.Create(fpath).Close();

            FileStream fs = new FileStream(fpath, FileMode.Append, FileAccess.Write);
            StreamWriter SWriter = new StreamWriter(fs);
            SWriter.WriteLine("--------Error Log --- Time:" + DateTime.Now.ToString("dd-MMM-yy") + " -" + DateTime.Now.ToString("T") + "----" + new UserCredentials().GetUser(UserName).LastName.ToString() + "--------" + method + "---" + param);
            SWriter.WriteLine("");
            SWriter.WriteLine(exception);
            SWriter.WriteLine("");
            SWriter.Flush();
            SWriter.Close();
        }

        public string GetJson(DataTable dt)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.Indented);
        }

        public string MakeBlkstring(string val)
        {
            if (val == null)
            {
                val = "";
            }
            return val;
        }

        public DataTable RenameColumns(DataTable dt, string SectionName)
        {
            NameValueCollection DBColumnMap = System.Configuration.ConfigurationManager.GetSection("DBColumnMap/" + SectionName) as NameValueCollection;

            foreach (DataColumn dc in dt.Columns)
            {
                foreach (string DBColumnMapValue in DBColumnMap)
                {
                    if (dc.ColumnName == DBColumnMapValue)
                    {
                        dc.ColumnName = DBColumnMap[dc.ColumnName];
                        break;
                    }
                }
            }

            return dt;
        }

    }
}