﻿using System;
using eLedgerBusiness.DAL;
using System.Collections;

namespace eLedgerBusiness
{
    public class ActivityBusiness
    {
        public string getActivity(string uln)
        {
            try
            {
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);
                return new WashDAL().getJsonFromQuery("Activity", paramText, paramValues);
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "Activity", "ULN"+uln);
                return null;
            }
        }
    }
}