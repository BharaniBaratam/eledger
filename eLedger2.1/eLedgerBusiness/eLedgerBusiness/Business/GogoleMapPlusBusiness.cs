﻿using System;
using System.Web;

using System.Data;
using eLedgerBusiness.DAL;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Collections.Generic;
using eLedgerEntities;

namespace eLedgerBusiness
{
    public class GogoleMapPlusBusiness
    {

        public string googleMapplus(string hostname)
        {

            string googlemapkey = eLedgerBusiness.Utils.ReadWebConfig.GoogleMapKey(hostname);
            return googlemapkey;
        }


        public DataTable getexporttoexcel(string uln, string loctypevalu, string distance)
        {
            try
            {
                DataTable dt;
                string LMSurveyLink = System.Configuration.ConfigurationManager.AppSettings["LMSurveyLink"].ToString();
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == null || UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                UserName = new UserCredentials().GetUser(UserName).LastName.ToString();
                string spName = "getLMSurvey";
                switch (loctypevalu)
                {
                    case "All":
                        loctypevalu = "1";
                        break;
                    case "LocationCenter":
                        loctypevalu = "2";
                        break;
                    case "LocationNearBy":
                        loctypevalu = "3";
                        break;
                    case "Laundromat":
                        loctypevalu = "4";
                        break;
                    default:
                        loctypevalu = "2";
                        break;
                }
                if (distance == "5")
                    distance = "0.5";
                if (distance == "10")
                    distance = "1.0";
                if (distance == "20")
                    distance = "2.0";
                ArrayList paramText = new ArrayList();
                paramText.Add("@locationType");
                paramText.Add("@uln");
                paramText.Add("@distance");
                paramText.Add("@user");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(loctypevalu);
                paramValues.Add(uln);
                paramValues.Add(distance);
                paramValues.Add(UserName);
                dt = new WashDAL().getDataTable(spName, paramText, paramValues);
                //jsondata = GetJson(dt);
                //return jsondata;
                return dt;
            }catch(Exception Ex)
            {
                new WashDAL().CreateLog(Ex.ToString(), "", "uln:" + uln + "loctypevalu:" + loctypevalu + "distance:" + distance);
                return null;
            }
        }


        public string getlatlongvalue(string uln, string locTypeValue, string distanceValue)
        {

            DataSet locInfo = null;
            string locNum = uln.Trim().ToUpper().Replace("-", "");
            string outVal = "nolocs";
            string LMSurveyLink = System.Configuration.ConfigurationManager.AppSettings["LMSurveyLink"].ToString();
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            if (UserName == null || UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            UserName = new UserCredentials().GetUser(UserName).LastName.ToString();
            switch (locTypeValue)
            {
                case "All":
                    locTypeValue = "1";
                    break;
                case "LocationCenter":
                    locTypeValue = "2";
                    break;
                case "LocationNearBy":
                    locTypeValue = "3";
                    break;
                case "Laundromat":
                    locTypeValue = "4";
                    break;
                default:
                    locTypeValue = "2";
                    break;
            }
            try
            {
                if (distanceValue == "5")
                    distanceValue = "0.5";
                if (distanceValue == "10")
                    distanceValue = "1.0";
                if (distanceValue == "20")
                    distanceValue = "2.0";

                ArrayList paramText = new ArrayList();
                paramText.Add("@locationType");
                paramText.Add("@uln");
                paramText.Add("@distance");
                paramText.Add("@user");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(locTypeValue);
                paramValues.Add(uln);
                paramValues.Add(distanceValue);
                paramValues.Add(UserName);
                int count = 0;
                locInfo = new WashDAL().getAgmtInfoDS("getLMSurvey", paramText, paramValues);
                string jsondata = "";
                if (locInfo.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in locInfo.Tables[0].Rows) // Loop over the rows..
                    {
                        count++;
                        if (jsondata != "")
                        {
                            if (Convert.ToInt32(row["locationType"].ToString()) == 4)
                            {
                                jsondata = jsondata + ",{\"ULN\" : \"" + row["LocID"] + "\",\"Latitude\" : \"" + row["latitude"] + "\",\"Logitude\" : \"" + row["Longitude"] + "\",\"Address\" : \"" + "<a href=" + LMSurveyLink + row["LocID"] + " Target=_blank>" + formatUln(row["LocID"].ToString()) + "</a></br>" + row["Address"] + "</br>" + row["City"] + "," + row["State"] + " " + row["Zip"] + "</b></br>As of:" + row["SurveyedOn"] + " - Dst: " + row["Distance"] + "m</br>" + row["WD"] + "\"}";
                            }
                            else
                            {
                                jsondata = jsondata + ",{\"ULN\" : \"" + row["LocID"] + "\",\"Latitude\" : \"" + row["latitude"] + "\",\"Logitude\" : \"" + row["Longitude"] + "\",\"Address\" : \"" + "<b>" + formatUln(row["LocID"].ToString()) + "</b></br>" + row["Address"] + "</br>" + row["City"] + "," + row["State"] + " " + row["Zip"] + "</b></br>Route :" + row["Route"] + "</br>" + row["WD"] + "\"}";
                            }
                        }
                        else
                            jsondata = "{\"ULN\" : \"" + row["LocID"] + "\",\"Latitude\" : \"" + row["latitude"] + "\",\"Logitude\" : \"" + row["Longitude"] + "\",\"Address\" : \"" + "<b>" + formatUln(row["LocID"].ToString()) + "</b></br>" + row["Address"] + "</br>" + row["City"] + "," + row["State"] + " " + row["Zip"] + "</b></br>Route :" + row["Route"] + "</br>" + row["WD"] + "\"}";
                    }
                }
                outVal = "[" + jsondata + "]";
            }
            catch (Exception ex)
            {
                string x = ex.Message;

            }
            return outVal;
        }
        private string formatUln(string ULN)
        {

            ULN = ULN.Replace("-", "");
            if (ULN.Length == 9)
            {
                ULN = ULN.Substring(0, 4) + '-' + ULN.Substring(4, 2) + '-' + ULN.Substring(6, 3);

            }
            return ULN;
        }
    }

}
