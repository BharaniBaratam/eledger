﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using System.Data;
using eLedgerBusiness.DAL;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
namespace eLedgerBusiness
{
    public class NotesBusiness
    {
        DataTable dt;
        string UserName;
        User userdet = new User();

        public string formatnotetext(string notetext)
        {
            if (notetext.Contains("\\n"))
                notetext = notetext.Replace("\\n", "<br />");
            return notetext;
        }
        public string AddNewNote(string uln, string noteText)
        {
            try
            {
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userdet = new UserCredentials().GetUser(UserName);
                int result = 0;
                string sourceCode = "ELED";
                string userId = userdet.UserID.ToString();
                string queryCode;
                queryCode = "addNewNote";

                DataTable dt;
                ArrayList paramText = new ArrayList();
                paramText.Add("@CreatedDt");
                paramText.Add("@CreatedById");
                paramText.Add("@UpdatedDt");
                paramText.Add("@UpdatedById");
                paramText.Add("@KeyId");
                paramText.Add("@SourceCd");
                paramText.Add("@NoteText");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(DateTime.Today);
                paramValues.Add(userId);
                paramValues.Add(DateTime.Today);
                paramValues.Add(userId);
                paramValues.Add(uln);
                paramValues.Add(sourceCode);
                paramValues.Add(noteText);
                result = new WashDAL().executeQuery(queryCode, paramText, paramValues);


                string noteins = "true";
                return noteins;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "AddNotes", "uln:" + uln + " noteText:" + noteText);
                return null;
            }
        }
        public string SubmitUpdate(string uln, string id, string noteText, string visibilityCode, string threadClosed)
        {
            try
            {
                string updatenote;
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == null || UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                int userId = new UserCredentials().GetUser(UserName).UserID;
                int result;
                string querycode;
                querycode = "updateNote";
                ArrayList paramText = new ArrayList();
                paramText.Add("@UpdatedById");
                paramText.Add("@NoteText");
                paramText.Add("@vis");
                paramText.Add("@closed");
                paramText.Add("@ID");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(userId);
                paramValues.Add(noteText);
                paramValues.Add("0");
                paramValues.Add("0");
                paramValues.Add(id);
                result = new WashDAL().executeQuery(querycode, paramText, paramValues);

                updatenote = "true";


                return updatenote;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "UpdateNotes", "uln:" + uln + " id:" + id + " noteText:" + noteText + " visibilityCode:" + visibilityCode + " threadClosed:" + threadClosed);
                return null;
            }
        }
        public string updatedeleteNotes(string uln, string id, string notetext)
        {
            try
            {
                string querycode, NotesList;
                querycode = "updateNote_delete";
                int result;
                ArrayList paramText = new ArrayList();
                paramText.Add("@ID");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(Convert.ToInt64(id));
                result = new WashDAL().executeQuery(querycode, paramText, paramValues);
                NotesList = "true";

                return NotesList;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "UpdatedeleteNotes", "uln:" + uln + " id:" + id + " notetext:" + notetext);
                return null;
            }
        }
        public string validateId(string id)
        {
            try
            {

                ArrayList paramValues = new ArrayList();
                DataTable dt;

                string queryCodeSummary = "getNoteById";

                ArrayList paramText = new ArrayList();

                paramText.Add("@ID");

                paramValues.Add(id);

                dt = new WashDAL().getDataTable(queryCodeSummary, paramText, paramValues);

                string jsondata = "";
                jsondata = new WashDAL().GetJson(dt);
                return jsondata;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "NotesvalidateID", "ID:" + id);
                return null;
            }
        }

        public string getJsonNotes(string uln)
        {
            try
            {
                string json;
                int canEdit;
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@SC");
                ArrayList paramValues = new ArrayList();
                paramValues.Add(uln);
                paramValues.Add("ELED");
                string queryCode = "geteLedgerNotes";
                DataTable dtnotes = new WashDAL().getDataTable(queryCode, paramText, paramValues);
                dtnotes.TableName = "Notes";

                DateTime referenceDate = DateTime.Today;
                int daysAllowed = eLedgerBusiness.Utils.ReadWebConfig.DaysAllowedToDeleteNote();
                UserName = System.Web.HttpContext.Current.User.Identity.Name;
                if (UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userdet = new UserCredentials().GetUser(UserName);

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new
                 System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows =
                  new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;

                foreach (DataRow dr in dtnotes.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtnotes.Columns)
                    {
                        row.Add(col.ColumnName.Trim(), dr[col].ToString());
                    }
                    try
                    {
                        int createdById = (int)dr["CreatedById"];
                        DateTime noteDate = (DateTime)dr["NoteDate"];

                        canEdit = (createdById == userdet.UserID
                                    && referenceDate.Date < noteDate.AddDays(daysAllowed).Date) ? 1 : 0;
                    }
                    catch (Exception)
                    {
                        canEdit = 0;
                    }
                    row.Add("canEdit", canEdit);
                    rows.Add(row);
                }
                json = serializer.Serialize(rows);

                return formatnotetext(json);
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "GetNotes", "uln:" + uln);
                return null;
            }
        }
    }
}
        


