﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using System.Data;


using eLedgerBusiness.DAL;
using System.Net;

using System.Collections;
using System.Data.SqlClient;
using System.Text;
using Newtonsoft.Json;

namespace eLedgerBusiness
{
    public class ContactBusiness
    {
        public string formatnotetext(string notetext)
        {
            if (notetext.Contains("\\n"))
                notetext = notetext.Replace("\\n", "<br />");
            return notetext;
        }

        public string getContactOverviewJson(string Id, string Type)
        {
            try
            {
                string sql, sql1, sql3;
                ArrayList paramText, paramText1, paramValues, paramText2, paramValues2;
                sql = "contactSummary";
                sql1 = "contactMasterCardSummary";
                paramText = new ArrayList();
                paramText1 = new ArrayList();
                paramText2 = new ArrayList();

                paramText.Add("@Id");
                paramText1.Add("@MCID");
                paramValues = new ArrayList();
                paramValues2 = new ArrayList();
                paramValues.Add(Id);
                
                if (Type == "")
                {
                    paramText2.Add("@Id");
                    paramValues2.Add(Id);
                    sql3 = "getRelatedLoc";
                }
                else
                {
                    paramText2.Add("@Id");
                    paramText2.Add("@Type");
                    paramValues2.Add(Id);
                    paramValues2.Add(Type);
                    sql3 = "getMasterPayeeContactDet";
                }

                DataTable dtrelCon = new WashDAL().getDataTable(sql3, paramText2, paramValues2);
                DataTable dtable = new WashDAL().getDataTable(sql, paramText, paramValues);
                DataTable dt1 = new WashDAL().getDataTable(sql1, paramText1, paramValues);

                string Seriliazedatat = "";

                dtable.TableName = "ContactSummary";
                dt1.TableName = "ContactMaster";
                dtrelCon.TableName = "RelatedContacts";
                DataSet ds = new DataSet();

                
                ds.Tables.Add(dtable);
                dtable.Columns.Add("CMSURL", typeof(String));
                dtable.Rows[0]["CMSURL"] = System.Configuration.ConfigurationManager.AppSettings["CMS"] + Id;
                ds.Tables.Add(dt1);
                ds.Tables.Add(dtrelCon);

                Seriliazedatat = Newtonsoft.Json.JsonConvert.SerializeObject(ds, Formatting.Indented);

                string Json = Seriliazedatat;
                if (Json.Length > 0)
                    Json = Json.Substring(0, Json.Length - 1);
                Json += ",\"ContactSummaryCount\":\"" + dtable.Rows.Count + "\" " + ",\"ContactMasterCount\":\"" + dt1.Rows.Count + "\",\"RelatedContactCount\":\"" + dtrelCon.Rows.Count + "\"}";


                return Json;
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "ContactOverview", "Id:" + Id + "Type:" + Type);
                return null;
            }
        }


        public string formattedULN(string ULN)
        {
            if (ULN.Length > 8)
            {
                ULN = ULN.Insert(4, "-");
                ULN = ULN.Insert(7, "-");                
                return ULN;
            }
            else
                return ULN;
        }
        public string formattedphone(string phno)
        {
            if (phno.Length > 8)
            {
                phno = phno.Insert(0, "(");
                phno = phno.Insert(4, ") ");
                phno = phno.Insert(9, "-");                
                return phno;
            }
            else
                return phno;
        }
      
        public string getlocContacts(string uln)
        {
            try
            {
                ArrayList paramValues = new ArrayList();
                string queryCode = "getlocContacts";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);

                DataTable dt = new WashDAL().getDataTable("getlocContacts", paramText, paramValues);

                return Newtonsoft.Json.JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            catch (Exception ex)
            {
                new WashDAL().CreateLog(ex.ToString(), "loccontact", "ULN:" + uln);
                return null;
            }
        }
        protected void MakeBlankRow(ref DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
        }
    }
}