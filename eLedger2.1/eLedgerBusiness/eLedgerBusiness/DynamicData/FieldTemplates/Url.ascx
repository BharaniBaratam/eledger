﻿<%@ Control Language="C#" CodeBehind="Url.ascx.cs" Inherits="eLedgerBusiness.UrlField" %>

<asp:HyperLink ID="HyperLinkUrl" runat="server" Text="<%# FieldValueString %>" Target="_blank" />

