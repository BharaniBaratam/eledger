﻿using System;
using System.Web;

using System.Data;
using eLedgerBusiness.DAL;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Collections.Generic;
using eLedgerEntities;

namespace eLedgerBusiness
{
    public class AWSGogoleMapPlusBusiness
    {

        public string googleMapplus(string hostname)
        {

            string googlemapkey = eLedgerBusiness.Utils.ReadWebConfig.GoogleMapKey(hostname);
            return googlemapkey;
        }


        public DataTable getexporttoexcel(string uln, string loctypevalu, string distance)
        {
            try
            {
                DataTable dt, dtUnSorted;
                string LMSurveyLink = System.Configuration.ConfigurationManager.AppSettings["LMSurveyLink"].ToString();
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                //string UserName = "WASHLAUNDRY\\bbaratam";
                if (UserName == null || UserName == "")
                    UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                //UserName = new UserCredentials().GetUser(UserName).LastName.ToString();
                string spName = "getLMSurvey";
                double distance1 = Convert.ToDouble(distance);
                switch (loctypevalu)
                {
                    case "All":
                        loctypevalu = "1";
                        if (distance1 == 0)
                            spName = "getLMSurvey_LocType1_0";
                        else if (distance1 == 5)
                            spName = "getLMSurvey_LocType1_0_5";
                        else if (distance1 == 10)
                            spName = "getLMSurvey_LocType1_1";
                        else if (distance1 == 20)
                            spName = "getLMSurvey_LocType1_2";
                        break;
                    case "LocationCenter":
                        loctypevalu = "2";
                        spName = "getLMSurvey_LocType2";
                        break;
                    case "LocationNearBy":
                        loctypevalu = "3";
                        if (distance1 == 0)
                            spName = "getLMSurvey_LocType3_0";
                        else if (distance1 == 5)
                            spName = "getLMSurvey_LocType3_0_5";
                        else if (distance1 == 10)
                            spName = "getLMSurvey_LocType3_1";
                        else if (distance1 == 20)
                            spName = "getLMSurvey_LocType3_2";
                        break;
                    case "Laundromat":
                        loctypevalu = "4";
                        spName = "getLMSurvey_LocType4";
                        break;
                    default:
                        loctypevalu = "2";
                        spName = "getLMSurvey_LocType2";
                        break;
                }

                
                if (distance1 == 5)
                    distance1 = 0.5;
                if (distance1 == 10)
                    distance1 = 1.0;
                if (distance1 == 20)
                    distance1 = 2.0;

                if (distance1 == 0)
                    spName = "getLMSurvey_LocType2";

                ArrayList paramText = new ArrayList();
                paramText.Add("@locationType");
                paramText.Add("@uln");
                paramText.Add("@distance");
                paramText.Add("@user");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(loctypevalu);
                paramValues.Add(uln);
                paramValues.Add(distance1);
                paramValues.Add(UserName);
                dtUnSorted = new WashAWSDAL().getDataTable(spName, paramText, paramValues);
                dtUnSorted = new WashAWSDAL().RenameColumns(dtUnSorted, "GetLMSurvey");

                DataView dv = dtUnSorted.DefaultView;
                dv.Sort = "Distance ASC";
                dt = dv.ToTable();

                //jsondata = GetJson(dt);
                //return jsondata;
                return dt;
            }catch(Exception Ex)
            {
                new WashAWSDAL().CreateLog(Ex.ToString(), "", "uln:" + uln + "loctypevalu:" + loctypevalu + "distance:" + distance);
                return null;
            }
        }


        public string getlatlongvalue(string uln, string locTypeValue, string distanceValue)
        {

            DataSet locInfo = null;
            
            DataTable locInfoDT;
            string locNum = uln.Trim().ToUpper().Replace("-", "");
            string outVal = "nolocs";
            string LMSurveyLink = System.Configuration.ConfigurationManager.AppSettings["LMSurveyLink"].ToString();
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            //string UserName = "WASHLAUNDRY\\bbaratam";
            if (UserName == null || UserName == "")
                UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //UserName = new UserCredentials().GetUser(UserName).LastName.ToString();
            double distanceValue1 = Convert.ToDouble(distanceValue);
            string spName = "getLMSurvey";
            switch (locTypeValue)
            {
                case "All":
                    locTypeValue = "1";
                    if(distanceValue1 == 0)
                        spName = "getLMSurvey_LocType1_0";
                    else if(distanceValue1 == 5)
                        spName = "getLMSurvey_LocType1_0_5";
                    else if (distanceValue1 == 10)
                        spName = "getLMSurvey_LocType1_1";
                    else if (distanceValue1 == 20)
                        spName = "getLMSurvey_LocType1_2";
                    break;
                case "LocationCenter":
                    locTypeValue = "2";
                    spName = "getLMSurvey_LocType2";
                    break;
                case "LocationNearBy":
                    locTypeValue = "3";
                    if (distanceValue1 == 0)
                        spName = "getLMSurvey_LocType3_0";
                    else if (distanceValue1 == 5)
                        spName = "getLMSurvey_LocType3_0_5";
                    else if (distanceValue1 == 10)
                        spName = "getLMSurvey_LocType3_1";
                    else if (distanceValue1 == 20)
                        spName = "getLMSurvey_LocType3_2";
                    break;
                case "Laundromat":
                    locTypeValue = "4";
                    spName = "getLMSurvey_LocType4";
                    break;
                default:
                    locTypeValue = "2";
                    spName = "getLMSurvey_LocType2";
                    break;
            }
            try
            {
                if (distanceValue1 == 5)
                    distanceValue1 = 0.5;
                if (distanceValue1 == 10)
                    distanceValue1 = 1.0;
                if (distanceValue1 == 20)
                    distanceValue1 = 2.0;

                if (distanceValue1 == 0)
                    spName = "getLMSurvey_LocType2";

                ArrayList paramText = new ArrayList();
                paramText.Add("@locationType");
                paramText.Add("@uln");
                paramText.Add("@distance");
                paramText.Add("@user");

                ArrayList paramValues = new ArrayList();
                paramValues.Add(locTypeValue);
                paramValues.Add(uln);
                paramValues.Add(distanceValue1);
                paramValues.Add(UserName);
                int count = 0;

                //locInfoDT = new WashAWSDAL().getDataTable("getLMSurvey", paramText, paramValues);
                locInfo = new WashAWSDAL().getAgmtInfoDS(spName, paramText, paramValues);

       

                locInfo.Tables[0].Columns["locid"].ColumnName = "LocId";
                locInfo.Tables[0].Columns["locname"].ColumnName = "LocName";
                locInfo.Tables[0].Columns["distance"].ColumnName = "distance";
                locInfo.Tables[0].Columns["address"].ColumnName = "Address";
                locInfo.Tables[0].Columns["city"].ColumnName = "City";
                locInfo.Tables[0].Columns["state"].ColumnName = "State";
                locInfo.Tables[0].Columns["zip"].ColumnName = "Zip";
                locInfo.Tables[0].Columns["branchcode"].ColumnName = "BranchCode";
                locInfo.Tables[0].Columns["route"].ColumnName = "Route";
                locInfo.Tables[0].Columns["routeday"].ColumnName = "RouteDay";
                locInfo.Tables[0].Columns["latitude"].ColumnName = "Latitude";
                locInfo.Tables[0].Columns["longitude"].ColumnName = "Longitude";
                locInfo.Tables[0].Columns["locationtype"].ColumnName = "LocationType";
                locInfo.Tables[0].Columns["wd"].ColumnName = "WD";
                locInfo.Tables[0].Columns["washer"].ColumnName = "Washer";
                locInfo.Tables[0].Columns["dryer"].ColumnName = "Dryer";
                locInfo.Tables[0].Columns["drytime"].ColumnName = "DryTime";
                locInfo.Tables[0].Columns["surveyedon"].ColumnName = "SurveyedOn";
                locInfo.Tables[0].Columns["createddate"].ColumnName = "CreatedDate";
                locInfo.Tables[0].Columns["createddate_fmt"].ColumnName = "CreatedDate_Fmt";
                locInfo.Tables[0].Columns["createdbyid"].ColumnName = "CreatedById";
                locInfo.Tables[0].Columns["createdby"].ColumnName = "CreatedBy";
                locInfo.Tables[0].Columns["updateddate"].ColumnName = "UpdatedDate";
                locInfo.Tables[0].Columns["updateddate_fmt"].ColumnName = "UpdatedDate_Fmt";
                locInfo.Tables[0].Columns["updatedbyid"].ColumnName = "UpdatedById";
                locInfo.Tables[0].Columns["updatedby"].ColumnName = "UpdatedBy";
                locInfo.Tables[0].Columns["deactivateddate"].ColumnName = "DeactivatedDate";
                locInfo.Tables[0].Columns["deactivateddate_fmt"].ColumnName = "DeactivatedDate_Fmt";
                locInfo.Tables[0].Columns["uln"].ColumnName = "ULN";
                locInfo.Tables[0].Columns["gplocindex"].ColumnName = "GPLocIndex";
                locInfo.Tables[0].Columns["propertytype"].ColumnName = "PropertyType";
                locInfo.Tables[0].Columns["status"].ColumnName = "Status";
                locInfo.Tables[0].Columns["propertyname"].ColumnName = "PropertyName";
                locInfo.Tables[0].Columns["address"].ColumnName = "Address";
                locInfo.Tables[0].Columns["city"].ColumnName = "City";
                locInfo.Tables[0].Columns["state"].ColumnName = "State";
                locInfo.Tables[0].Columns["zip"].ColumnName = "Zip";
                locInfo.Tables[0].Columns["county"].ColumnName = "County";
                locInfo.Tables[0].Columns["phone1"].ColumnName = "Phone1";
                locInfo.Tables[0].Columns["phone2"].ColumnName = "Phone2";
                locInfo.Tables[0].Columns["fax"].ColumnName = "Fax";
                locInfo.Tables[0].Columns["longitude"].ColumnName = "Longitude";
                locInfo.Tables[0].Columns["longitude_fmt"].ColumnName = "Longitude_Fmt";
                locInfo.Tables[0].Columns["latitude"].ColumnName = "Latitude";
                locInfo.Tables[0].Columns["latitude_fmt"].ColumnName = "Latitude_Fmt";
                locInfo.Tables[0].Columns["branchcode"].ColumnName = "BranchCode";
                locInfo.Tables[0].Columns["branch"].ColumnName = "Branch";
                locInfo.Tables[0].Columns["servicegroup"].ColumnName = "ServiceGroup";
                locInfo.Tables[0].Columns["timezone"].ColumnName = "TimeZone";
                locInfo.Tables[0].Columns["timezone_fmt"].ColumnName = "TimeZone_Fmt";
                locInfo.Tables[0].Columns["dst"].ColumnName = "DST";
                locInfo.Tables[0].Columns["proximityid"].ColumnName = "ProximityId";
                locInfo.Tables[0].Columns["proximity"].ColumnName = "Proximity";
                locInfo.Tables[0].Columns["mentoservice"].ColumnName = "MenToService";
                locInfo.Tables[0].Columns["risk"].ColumnName = "Risk";
                locInfo.Tables[0].Columns["apartmentunits"].ColumnName = "ApartmentUnits";
                locInfo.Tables[0].Columns["apartmentunits_fmt"].ColumnName = "ApartmentUnits_Fmt";
                locInfo.Tables[0].Columns["residentsidebyside"].ColumnName = "ResidentSideBySide";
                locInfo.Tables[0].Columns["residentsidebyside_fmt"].ColumnName = "ResidentSideBySide_Fmt";
                locInfo.Tables[0].Columns["residentsidebysidefurnished"].ColumnName = "ResidentSideBySideFurnished";
                locInfo.Tables[0].Columns["residentsidebysidefurnished_fmt"].ColumnName = "ResidentSideBySideFurnished_Fmt";
                locInfo.Tables[0].Columns["residentministack"].ColumnName = "ResidentMiniStack";
                locInfo.Tables[0].Columns["residentministack_fmt"].ColumnName = "ResidentMiniStack_Fmt";
                locInfo.Tables[0].Columns["residentministackfurnished"].ColumnName = "ResidentMiniStackFurnished";
                locInfo.Tables[0].Columns["residentministackfurnished_fmt"].ColumnName = "ResidentMiniStackFurnished_Fmt";
                locInfo.Tables[0].Columns["firstinstalldate"].ColumnName = "FirstInstallDate";
                locInfo.Tables[0].Columns["firstinstalldate_fmt"].ColumnName = "FirstInstallDate_Fmt";
                locInfo.Tables[0].Columns["contracttype"].ColumnName = "ContractType";
                locInfo.Tables[0].Columns["billingmethod"].ColumnName = "BillingMethod";
                locInfo.Tables[0].Columns["billingmethodshort"].ColumnName = "BillingMethodShort";
                locInfo.Tables[0].Columns["commissioncode"].ColumnName = "CommissionCode";
                locInfo.Tables[0].Columns["commissiontype"].ColumnName = "CommissionType";
                locInfo.Tables[0].Columns["vendingmethod"].ColumnName = "VendingMethod";
                locInfo.Tables[0].Columns["vendingmethodavs"].ColumnName = "VendingMethodAVS";
                locInfo.Tables[0].Columns["expirationdate"].ColumnName = "ExpirationDate";
                locInfo.Tables[0].Columns["expirationdate_fmt"].ColumnName = "ExpirationDate_Fmt";
                locInfo.Tables[0].Columns["managername"].ColumnName = "ManagerName";
                locInfo.Tables[0].Columns["manageraptnum"].ColumnName = "ManagerAptNum";
                locInfo.Tables[0].Columns["route"].ColumnName = "Route";
                locInfo.Tables[0].Columns["routeday"].ColumnName = "RouteDay";
                locInfo.Tables[0].Columns["cycledays"].ColumnName = "CycleDays";
                locInfo.Tables[0].Columns["cycledays_fmt"].ColumnName = "CycleDays_Fmt";
                locInfo.Tables[0].Columns["subcycle"].ColumnName = "SubCycle";
                locInfo.Tables[0].Columns["cycleinfo_fmt"].ColumnName = "CycleInfo_Fmt";
                locInfo.Tables[0].Columns["mappage"].ColumnName = "MapPage";
                locInfo.Tables[0].Columns["mapgrid"].ColumnName = "MapGrid";
                locInfo.Tables[0].Columns["mapinfo_fmt"].ColumnName = "MapInfo_Fmt";
                locInfo.Tables[0].Columns["timestart"].ColumnName = "TimeStart";
                locInfo.Tables[0].Columns["timestart_fmt"].ColumnName = "TimeStart_Fmt";
                locInfo.Tables[0].Columns["timeend"].ColumnName = "TimeEnd";
                locInfo.Tables[0].Columns["timeend_fmt"].ColumnName = "TimeEnd_Fmt";
                locInfo.Tables[0].Columns["lockbox"].ColumnName = "LockBox";
                locInfo.Tables[0].Columns["lockbox_fmt"].ColumnName = "LockBox_Fmt";
                locInfo.Tables[0].Columns["slipprinted"].ColumnName = "SlipPrinted";
                locInfo.Tables[0].Columns["slipprinted_fmt"].ColumnName = "SlipPrinted_Fmt";
                locInfo.Tables[0].Columns["specialinstructions"].ColumnName = "SpecialInstructions";
                locInfo.Tables[0].Columns["previousoperator"].ColumnName = "PreviousOperator";
                locInfo.Tables[0].Columns["competitorname"].ColumnName = "CompetitorName";
                locInfo.Tables[0].Columns["maxpod"].ColumnName = "MaxPOD";
                locInfo.Tables[0].Columns["laundryrooms"].ColumnName = "LaundryRooms";
                locInfo.Tables[0].Columns["laundryrooms_fmt"].ColumnName = "LaundryRooms_Fmt";
                locInfo.Tables[0].Columns["machinesactual"].ColumnName = "MachinesActual";
                locInfo.Tables[0].Columns["machinesactual_fmt"].ColumnName = "MachinesActual_Fmt";
                locInfo.Tables[0].Columns["machinescountas"].ColumnName = "MachinesCountAs";
                locInfo.Tables[0].Columns["machinescountas_fmt"].ColumnName = "MachinesCountAs_Fmt";
                locInfo.Tables[0].Columns["machinescontract"].ColumnName = "MachinesContract";
                locInfo.Tables[0].Columns["machinescontract_fmt"].ColumnName = "MachinesContract_Fmt";
                locInfo.Tables[0].Columns["adjgross30washavg"].ColumnName = "AdjGross30WashAvg";
                locInfo.Tables[0].Columns["adjgross30washavg_fmt"].ColumnName = "AdjGross30WashAvg_Fmt";
                locInfo.Tables[0].Columns["adjgross30dryavg"].ColumnName = "AdjGross30DryAvg";
                locInfo.Tables[0].Columns["adjgross30dryavg_fmt"].ColumnName = "AdjGross30DryAvg_Fmt";
                locInfo.Tables[0].Columns["adjgross30totalavg"].ColumnName = "AdjGross30TotalAvg";
                locInfo.Tables[0].Columns["adjgross30totalavg_fmt"].ColumnName = "AdjGross30TotalAvg_Fmt";
                locInfo.Tables[0].Columns["geographicnetpermachine"].ColumnName = "GeographicNetPerMachine";
                locInfo.Tables[0].Columns["geographicnetpermachine_fmt"].ColumnName = "GeographicNetPerMachine_Fmt";
                locInfo.Tables[0].Columns["lastcollectiondate"].ColumnName = "LastCollectionDate";
                locInfo.Tables[0].Columns["lastcollectiondate_fmt"].ColumnName = "LastCollectionDate_Fmt";
                locInfo.Tables[0].Columns["lastservicedate"].ColumnName = "LastServiceDate";
                locInfo.Tables[0].Columns["lastservicedate_fmt"].ColumnName = "LastServiceDate_Fmt";
                locInfo.Tables[0].Columns["lastpricechangedate"].ColumnName = "LastPriceChangeDate";
                locInfo.Tables[0].Columns["lastpricechangedate_fmt"].ColumnName = "LastPriceChangeDate_Fmt";
                locInfo.Tables[0].Columns["predommfgwash"].ColumnName = "PredomMfgWash";
                locInfo.Tables[0].Columns["predommfgdry"].ColumnName = "PredomMfgDry";
                locInfo.Tables[0].Columns["predommfgavs"].ColumnName = "PredomMfgAVS";
                locInfo.Tables[0].Columns["predomsitecode"].ColumnName = "PredomSiteCode";
                locInfo.Tables[0].Columns["totphysicalcountaswash"].ColumnName = "TotPhysicalCountAsWash";
                locInfo.Tables[0].Columns["totphysicalcountaswash_fmt"].ColumnName = "TotPhysicalCountAsWash_Fmt";
                locInfo.Tables[0].Columns["totphysicalcountasdry"].ColumnName = "TotPhysicalCountAsDry";
                locInfo.Tables[0].Columns["totphysicalcountasdry_fmt"].ColumnName = "TotPhysicalCountAsDry_Fmt";
                locInfo.Tables[0].Columns["predomvendpricewash"].ColumnName = "PredomVendPriceWash";
                locInfo.Tables[0].Columns["predomvendpricewash_fmt"].ColumnName = "PredomVendPriceWash_Fmt";
                locInfo.Tables[0].Columns["predomvendpricedry"].ColumnName = "PredomVendPriceDry";
                locInfo.Tables[0].Columns["predomvendpricedry_fmt"].ColumnName = "PredomVendPriceDry_Fmt";
                locInfo.Tables[0].Columns["predomvendtimedry"].ColumnName = "PredomVendTimeDry";
                locInfo.Tables[0].Columns["predomvendtimedry_fmt"].ColumnName = "PredomVendTimeDry_Fmt";
                locInfo.Tables[0].Columns["managerphone"].ColumnName = "ManagerPhone";
                locInfo.Tables[0].Columns["noflyzone"].ColumnName = "NoFlyZone";
                locInfo.Tables[0].Columns["noflyzone_fmt"].ColumnName = "NoFlyZone_Fmt";
                locInfo.Tables[0].Columns["servicegoal"].ColumnName = "ServiceGoal";
                locInfo.Tables[0].Columns["servicegoal_fmt"].ColumnName = "ServiceGoal_Fmt";
                locInfo.Tables[0].Columns["routeacquiredid"].ColumnName = "RouteAcquiredId";
                locInfo.Tables[0].Columns["routeacquired"].ColumnName = "RouteAcquired";
                locInfo.Tables[0].Columns["companyid"].ColumnName = "CompanyId";
                locInfo.Tables[0].Columns["companyname"].ColumnName = "CompanyName";
                locInfo.Tables[0].Columns["defaultvndclsid"].ColumnName = "DefaultVndClsId";
                locInfo.Tables[0].Columns["lineofbusinessid"].ColumnName = "LineOfBusinessID";
                locInfo.Tables[0].Columns["lineofbusinessname"].ColumnName = "LineOfBusinessName";
                locInfo.Tables[0].Columns["countryid"].ColumnName = "CountryID";
                locInfo.Tables[0].Columns["country"].ColumnName = "Country";
                locInfo.Tables[0].Columns["apartment units"].ColumnName = "Apartment Units";
                locInfo.Tables[0].Columns["hookups"].ColumnName = "Hookups";
                locInfo.Tables[0].Columns["machines"].ColumnName = "Machines";
                locInfo.Tables[0].Columns["avg 30 d gross"].ColumnName = "Avg 30 D Gross";


                DataView dv = locInfo.Tables[0].DefaultView;
                dv.Sort = "distance ASC";
                DataTable dt = dv.ToTable();


                string jsondata = "";
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows) // Loop over the rows..
                    {
                        count++;
                        if (jsondata != "")
                        {
                            if (Convert.ToInt32(row["locationType"].ToString()) == 4)
                            {
                                jsondata = jsondata + ",{\"ULN\" : \"" + row["LocID"] + "\",\"Latitude\" : \"" + row["latitude"] + "\",\"Logitude\" : \"" + row["Longitude"] + "\",\"Address\" : \"" + "<a href=" + LMSurveyLink + row["LocID"] + " Target=_blank>" + formatUln(row["LocID"].ToString()) + "</a></br>" + row["Address"] + "</br>" + row["City"] + "," + row["State"] + " " + row["Zip"] + "</b></br>As of:" + row["SurveyedOn"] + " - Dst: " + row["Distance"] + "m</br>" + row["WD"] + "\"}";
                            }
                            else
                            {
                                jsondata = jsondata + ",{\"ULN\" : \"" + row["LocID"] + "\",\"Latitude\" : \"" + row["latitude"] + "\",\"Logitude\" : \"" + row["Longitude"] + "\",\"Address\" : \"" + "<b>" + formatUln(row["LocID"].ToString()) + "</b></br>" + row["Address"] + "</br>" + row["City"] + "," + row["State"] + " " + row["Zip"] + "</b></br>Route :" + row["Route"] + "</br>" + row["WD"] + "\"}";
                            }
                        }
                        else
                            jsondata = "{\"ULN\" : \"" + row["LocID"] + "\",\"Latitude\" : \"" + row["latitude"] + "\",\"Logitude\" : \"" + row["Longitude"] + "\",\"Address\" : \"" + "<b>" + formatUln(row["LocID"].ToString()) + "</b></br>" + row["Address"] + "</br>" + row["City"] + "," + row["State"] + " " + row["Zip"] + "</b></br>Route :" + row["Route"] + "</br>" + row["WD"] + "\"}";
                    }
                }
                outVal = "[" + jsondata + "]";
            }
            catch (Exception ex)
            {
                string x = ex.Message;

            }
            return outVal;
        }
        private string formatUln(string ULN)
        {

            ULN = ULN.Replace("-", "");
            if (ULN.Length == 9)
            {
                ULN = ULN.Substring(0, 4) + '-' + ULN.Substring(4, 2) + '-' + ULN.Substring(6, 3);

            }
            return ULN;
        }
    }

}
