﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Net;
using eLedgerBusiness.DAL;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

namespace eLedgerBusiness
{
    public class AWSEquipmentBusiness
    {
        string ULN = "";

        protected void MakeBlankRow(ref DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
        }
        public string getLocInventoryURL(string ULN)
        {
            string locInv = eLedgerBusiness.Utils.ReadWebConfig.LocInventory() + ULN;
            StringBuilder sbJson = new StringBuilder("{");
            sbJson.Append("\"LocInventoryURL\":[{");
            sbJson.AppendFormat("\"{0}\":\"{1}\" ", "URL", locInv);
            sbJson.Append("}]}");
            return sbJson.ToString();

        }

        public string getEquipmentJson(string ULN, string WebId, string level)
        {
            try
            {
                DataTable dt, dtUnSorted, dt1, dt2, dt2UnSorted;
                DataSet ds = new DataSet();
                string queryCode;


                queryCode = "equipmentCount";
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");

                paramValues.Add(ULN);
                ds = new WashAWSDAL().getAgmtInfoDSRetry(queryCode, paramText, paramValues, 0);

                dtUnSorted = ds.Tables[0];

                DataView dv = dtUnSorted.DefaultView;
                dv.Sort = "EquipmentType, LoadType, FuelType, Manufacturer, VendPrice_Fmt, CycleTime, EquipmentDesc";
                dt = dv.ToTable();

                dt1 = ds.Tables[1];
                string dt1_ActualTotal = dt.Compute("Sum(Actual)", "").ToString();
                string dt1_CountAsDryTotal = dt.Compute("Sum(CountAsDry)", "").ToString();
                string dt1_CountAsWashTotal = dt.Compute("Sum(CountAsWash)", "").ToString();
                string dt1_CountAsTotal = dt.Compute("Sum(CountAs)", "").ToString();

                dt2UnSorted = ds.Tables[2];

                DataView dv2 = dt2UnSorted.DefaultView;
                dv2.Sort = "EquipmentType, Manufacturer, VendingMethod, SiteCode, Dispensing, EquipmentDesc";
                dt2 = dv2.ToTable();

                string dt3_ActualTotal = dt2.Compute("Sum(Actual)", "").ToString();

                if (dt2.Rows.Count > 0 && dt.Rows.Count <= 0)
                {
                    MakeBlankRow(ref dt);
                }
                StringBuilder sbJson = new StringBuilder("{");
                if (ds != null)
                {
                    sbJson.Append("\"Equipment\":[");
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows) // Loop over the rows..
                        {
                            sbJson.Append("{");
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", row["EquipmentType"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Manufacturer", row["Manufacturer"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentDesc", row["EquipmentDesc"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Actual", row["Actual"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsDry", row["CountAsDry"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsWash", row["CountAsWash"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAs", row["CountAs"]);
                            if (row["FuelType"].ToString().Length > 0)
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FuelType", row["FuelType"]);

                            }
                            else
                            {
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FuelType", row["LoadType"]);
                            }
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CycleTime_Fmt", row["CycleTime_Fmt"]);
                            if (row["CycleTime_Fmt"].ToString() != "" && row["VendPrice_Fmt"].ToString() != "")
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "VendPrice_Fmt", row["VendPrice_Fmt"].ToString() + "/" + row["CycleTime_Fmt"].ToString());
                            else if (row["CycleTime_Fmt"].ToString() != "" && row["VendPrice_Fmt"].ToString() == "")
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "VendPrice_Fmt", row["CycleTime_Fmt"].ToString());
                            else
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "VendPrice_Fmt", row["VendPrice_Fmt"].ToString());
                            sbJson.Append("},");
                        }
                        if (dt1.Rows.Count > 0)
                        {
                            foreach (DataRow row1 in dt1.Rows) // Loop over the rows..
                            {
                                sbJson.Append("{");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", "N/A");
                                sbJson.AppendFormat("\"{0}\":\"\", ", "Manufacturer");
                                sbJson.AppendFormat("\"{0}\":\"\", ", "EquipmentDesc");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Actual", dt1_ActualTotal);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsDry", dt1_CountAsDryTotal);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsWash", dt1_CountAsWashTotal);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAs", dt1_CountAsTotal);
                                sbJson.AppendFormat("\"{0}\":\"\", ", "FuelType");
                                sbJson.AppendFormat("\"{0}\":\"\", ", "CycleTime_Fmt");
                                sbJson.AppendFormat("\"{0}\":\"\" ", "VendPrice_Fmt");
                                sbJson.Append("}");
                            }
                        }
                    }
                    sbJson.Append("],");
                    sbJson.Append("\"OtherEquipment\":[");
                    if (dt2.Rows.Count > 0)
                    {
                        foreach (DataRow row2 in dt2.Rows) // Loop over the rows..
                        {
                            sbJson.Append("{");
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Actual", row2["Actual"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", row2["EquipmentType"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Manufacturer", row2["Manufacturer"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentDesc", row2["EquipmentDesc"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendingMethod", row2["VendingMethod"]);
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Dispensing", row2["Dispensing"].ToString());
                            sbJson.AppendFormat("\"{0}\":\"{1}\" ", "SiteCode", row2["SiteCode"]);
                            sbJson.Append("},");
                        }
                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow row3 in ds.Tables[3].Rows) // Loop over the rows..
                            {
                                sbJson.Append("{");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Actual", dt3_ActualTotal);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", "N/A");
                                sbJson.AppendFormat("\"{0}\":\"\", ", "Manufacturer");
                                sbJson.AppendFormat("\"{0}\":\"\", ", "EquipmentDesc");
                                sbJson.AppendFormat("\"{0}\":\"\", ", "VendingMethod");
                                // sbJson.AppendFormat("\"{0}\":\"\", ", "Dispensing");
                                sbJson.AppendFormat("\"{0}\":\"\" ", "SiteCode");
                                //sbJson.AppendFormat("\"{0}\":\"{1}\"", "CountAsWash", row3["CountAsWash"]);  
                                sbJson.Append("}");
                            }
                        }
                    }
                    sbJson.Append("]");
                }
                sbJson.Append("}");
                ds.Dispose();
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "GetEquipment", "ULN:" + ULN + "WebId:" + WebId + "level:" + level);
                return null;
            }
        }

        public string getMachineListJson(string ULN)
        {
            try
            {
                string NoMachinesFlag = "true";
                DataSet ds = new DataSet();
                DataTable dtUnsorted, dtUnsorted1;
                string queryCode;
                queryCode = "equipmentMachines";
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(ULN);
                ds = new WashAWSDAL().getAgmtInfoDSRetry(queryCode, paramText, paramValues, 0);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;
                System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
                Dictionary<string, object> row1 = null;
                dtUnsorted = ds.Tables[0];
                dtUnsorted1 = ds.Tables[1];

                DataView dv = dtUnsorted.DefaultView;
                dv.Sort = "ULN, RoomId, EquipmentType, Manufacturer, VendPrice, VendPrice2, CycleTime, WebId";
                DataTable dt = dv.ToTable();

                DataView dv1 = dtUnsorted1.DefaultView;
                dv1.Sort = "RoomId";
                DataTable dt1 = dv1.ToTable();

                if (ds != null)
                {
                    if (dt1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt1.Rows) // Loop over the rows..
                        {
                            row = new Dictionary<string, object>();
                            row.Add("RoomID", dr["RoomID"].ToString());
                            row.Add("RoomName", dr["RoomName"].ToString());
                            if (dr["RoomName"].ToString() != "" && dr["Description"].ToString() == "" && dr["StreetAddress"].ToString() == "")
                            {
                                row.Add("Description", dr["RoomName"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            else if (dr["RoomName"].ToString() == "" && dr["Description"].ToString() != "" && dr["StreetAddress"].ToString() == "")
                            {
                                row.Add("Description", dr["Description"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            else if (dr["RoomName"].ToString() == "" && dr["Description"].ToString() == "" && dr["StreetAddress"].ToString() != "")
                            {
                                row.Add("Description", dr["StreetAddress"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            else if (dr["RoomName"].ToString() != "" && dr["Description"].ToString() == "" && dr["StreetAddress"].ToString() != "")
                            {
                                row.Add("Description", dr["RoomName"].ToString().Replace(System.Environment.NewLine, "") + " - " + dr["StreetAddress"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            else if (dr["RoomName"].ToString() != "" && dr["Description"].ToString() != "" && dr["StreetAddress"].ToString() == "")
                            {
                                row.Add("Description", dr["RoomName"].ToString().Replace(System.Environment.NewLine, "") + " - " + dr["Description"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            else if (dr["RoomName"].ToString() == "" && dr["Description"].ToString() != "" && dr["StreetAddress"].ToString() != "")
                            {
                                row.Add("Description", dr["StreetAddress"].ToString().Replace(System.Environment.NewLine, "") + " - " + dr["Description"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            else if (dr["RoomName"].ToString() != "" && dr["Description"].ToString() != "" && dr["StreetAddress"].ToString() != "")
                            {
                                row.Add("Description", dr["RoomName"].ToString().Replace(System.Environment.NewLine, "") + " - " + dr["StreetAddress"].ToString().Replace(System.Environment.NewLine, "") + " - " + dr["Description"].ToString().Replace(System.Environment.NewLine, ""));
                            }
                            row.Add("StreetAddress", dr["StreetAddress"].ToString());
                            row.Add("SpecialInstruction", dr["SpecialInstructions"].ToString().Replace(System.Environment.NewLine, ""));
                            row.Add("NumberofInstallers", dr["NumberofInstallers"].ToString());
                            row.Add("Size", dr["RoomSize"].ToString());
                            row.Add("ULN", ULN);
                            rows.Add(row);
                        }
                    }
                    if (dt.Rows.Count > 0)
                    {

                        foreach (DataRow dr1 in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            row1.Add("ULN", ULN);
                            row1.Add("WebId", dr1["WebId"].ToString());
                            row1.Add("RoomID", dr1["RoomID"].ToString());
                            row1.Add("EquipmentType", dr1["EquipmentType"].ToString());
                            row1.Add("Manufacturer", dr1["Manufacturer"].ToString());
                            row1.Add("Model", dr1["Model"].ToString().Trim());
                            row1.Add("PurchaseDate_Fmt", dr1["PurchaseDate_Fmt"].ToString());
                            row1.Add("AgeWhenPurchased_Fmt", dr1["AgeWhenPurchased_Fmt"].ToString());
                            row1.Add("LASTCERTDATE", dr1["LASTCERTDATE"].ToString());
                            row1.Add("LASTCERTSITE", dr1["LASTCERTSITE"].ToString());
                            row1.Add("Age_LASTCERTDATE_Fmt", dr1["Age_LASTCERTDATE_Fmt"].ToString());
                            row1.Add("LASTRECONDATE", dr1["LASTRECONDATE"].ToString());
                            row1.Add("LASTRECONSITE", dr1["LASTRECONSITE"].ToString());
                            row1.Add("Age_LASTRECONDATE_Fmt", dr1["Age_LASTRECONDATE_Fmt"].ToString());
                            row1.Add("TimeOnLoc_Fmt", dr1["TimeOnLoc_Fmt"].ToString());
                            row1.Add("CurrentCondition", dr1["CurrentCondition"].ToString());
                            row1.Add("AgeNow_Fmt", dr1["AgeNow_Fmt"].ToString());
                            row1.Add("CountAsWash_Fmt", dr1["CountAsWash_Fmt"].ToString());
                            row1.Add("CountAsDry_Fmt", dr1["CountAsDry_Fmt"].ToString());
                            row1.Add("MeterType", dr1["MeterType"].ToString());
                            row1.Add("Sleeved", dr1["Sleeved"].ToString());
                            row1.Add("VendingMethod", dr1["VendingMethod"].ToString());
                            row1.Add("FuelType", dr1["FuelType"].ToString());
                            row1.Add("VendPrice_Fmt", dr1["VendPrice_Fmt"].ToString());
                            row1.Add("CycleTime_Fmt", dr1["CycleTime_Fmt"].ToString());
                            row1.Add("LoadType", dr1["LoadType"].ToString());
                            row1.Add("InstallDate_Fmt", dr1["InstallDate_Fmt"].ToString());
                            rows1.Add(row1);
                        }
                    }

                }
                ds.Dispose();
                string Json = "{\"TotalLaundryMachine\":" + serializer.Serialize(rows) + ",\"TotalMachine\":" + serializer1.Serialize(rows1) + "}";
                return Json;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "EQMachine", "ULN:" + ULN);
                return null;
            }
        }

        public string getMachineListByWebIdJson(string uln, string webid)
        {
            try
            {
                DataSet ds = new DataSet();
                DataSet dsCopy = new DataSet();
                string queryCode;
                queryCode = "equipmentExtra";
                ArrayList paramValues = new ArrayList();

                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@WebId");

                paramValues.Add(uln);
                paramValues.Add(webid);
                ds = new WashAWSDAL().getAgmtInfoDSRetry(queryCode, paramText, paramValues, 0);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                { 
                    dsCopy = ds.Clone();
                    DataRow rowCopy = ds.Tables[0].Rows[0];
                    foreach (DataRow row1 in ds.Tables[0].Rows) // Loop over the rows..
                    {
                        if (row1["lastcertdate"].ToString() != "")
                            rowCopy["lastcertdate"] = row1["lastcertdate"];
                        if (row1["lastcertsite"].ToString() != "")
                            rowCopy["lastcertsite"] = row1["lastcertsite"];
                        if (row1["lastcertcntrd"].ToString() != "")
                            rowCopy["lastcertcntrd"] = row1["lastcertcntrd"];
                        if (row1["lastrecondate"].ToString() != "")
                            rowCopy["lastrecondate"] = row1["lastrecondate"];
                        if (row1["lastreconsite"].ToString() != "")
                            rowCopy["lastreconsite"] = row1["lastreconsite"];
                        if (row1["lastreconcntrd"].ToString() != "")
                            rowCopy["lastreconcntrd"] = row1["lastreconcntrd"];
                    }
                    //dsCopy.Tables[0].Rows.Add(rowCopy);
                    dsCopy.Tables[0].ImportRow(rowCopy);

                }

                StringBuilder sbJson = new StringBuilder("{");
                if (dsCopy != null)
                {
                    if (dsCopy.Tables[0].Rows.Count > 0)
                    {
                        sbJson.Append("\"MachineList\":[");
                        foreach (DataRow row in dsCopy.Tables[0].Rows) // Loop over the rows..
                        {
                            if (row["EquipmentType"].ToString() == "AVS")
                            {
                                sbJson.Append("{");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", row["EquipmentType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WebId", row["WebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Status", row["Status"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RoomId", row["RoomId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SerialNumber", row["SerialNumber"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Manufacturer", row["Manufacturer"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Model", row["Model"].ToString().Trim());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ContractType", row["ContractType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BillingMethod", row["BillingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FirstInstallDate_Fmt", row["FirstInstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeNow_Fmt", row["AgeNow_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InstallDate_Fmt", row["InstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TimeOnLoc_Fmt", row["TimeOnLoc_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseDate_Fmt", row["PurchaseDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeWhenPurchased_Fmt", row["AgeWhenPurchased_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONCNTRD", row["LASTRECONCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTCNTRD", row["LASTCERTCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTDATE", row["LASTCERTDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTSITE", row["LASTCERTSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONDATE", row["LASTRECONDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONSITE", row["LASTRECONSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCost_Fmt", row["PurchaseCost_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCondition", row["PurchaseCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CurrentCondition", row["CurrentCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "OwnedBy", row["OwnedBy"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ActualContract", row["ActualContract"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendingMethod", row["VendingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LoadType", row["LoadType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FuelType", row["FuelType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Voltage", row["Voltage"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MeterType", row["MeterType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Sleeved", row["Sleeved"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsWash_Fmt", row["CountAsWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsDry_Fmt", row["CountAsDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceWash_Fmt", row["VendPriceWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceDry_Fmt", row["VendPriceDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CycleTime_Fmt", row["CycleTime_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SpecialPrice_Fmt", row["SpecialPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCycle", row["SuperCycle"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCyclePrice_Fmt", row["SuperCyclePrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Topoff", row["Topoff"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TopoffPrice_Fmt", row["TopoffPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PrevWebId", row["PrevWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "NextWebId", row["NextWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentDesc", row["EquipmentDesc"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SiteCode", row["SiteCode"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "Dispensing", row["Dispensing"].ToString());
                                sbJson.Append("},");
                            }
                            else if ((row["EquipmentType"].ToString() == "Bill Changer") || (row["EquipmentType"].ToString() == "Soap Dispenser"))
                            {
                                sbJson.Append("{");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", row["EquipmentType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WebId", row["WebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Status", row["Status"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RoomId", row["RoomId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SerialNumber", row["SerialNumber"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Manufacturer", row["Manufacturer"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Model", row["Model"].ToString().Trim());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ContractType", row["ContractType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BillingMethod", row["BillingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FirstInstallDate_Fmt", row["FirstInstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeNow_Fmt", row["AgeNow_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InstallDate_Fmt", row["InstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TimeOnLoc_Fmt", row["TimeOnLoc_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseDate_Fmt", row["PurchaseDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeWhenPurchased_Fmt", row["AgeWhenPurchased_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONCNTRD", row["LASTRECONCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTCNTRD", row["LASTCERTCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTDATE", row["LASTCERTDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTSITE", row["LASTCERTSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONDATE", row["LASTRECONDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONSITE", row["LASTRECONSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCost_Fmt", row["PurchaseCost_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCondition", row["PurchaseCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CurrentCondition", row["CurrentCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "OwnedBy", row["OwnedBy"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ActualContract", row["ActualContract"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendingMethod", row["VendingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LoadType", row["LoadType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FuelType", row["FuelType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Voltage", row["Voltage"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MeterType", row["MeterType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Sleeved", row["Sleeved"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsWash_Fmt", row["CountAsWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsDry_Fmt", row["CountAsDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceWash_Fmt", row["VendPriceWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceDry_Fmt", row["VendPriceDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CycleTime_Fmt", row["CycleTime_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SpecialPrice_Fmt", row["SpecialPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCycle", row["SuperCycle"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCyclePrice_Fmt", row["SuperCyclePrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Topoff", row["Topoff"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TopoffPrice_Fmt", row["TopoffPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PrevWebId", row["PrevWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "NextWebId", row["NextWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentDesc", row["EquipmentDesc"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "VendPrice_Fmt", row["VendPrice_Fmt"].ToString());
                                sbJson.Append("},");

                            }
                            else if (row["EquipmentType"].ToString() == "Laundrimate")
                            {
                                sbJson.Append("{");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", row["EquipmentType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WebId", row["WebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Status", row["Status"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RoomId", row["RoomId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SerialNumber", row["SerialNumber"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Manufacturer", row["Manufacturer"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Model", row["Model"].ToString().Trim());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ContractType", row["ContractType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BillingMethod", row["BillingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FirstInstallDate_Fmt", row["FirstInstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeNow_Fmt", row["AgeNow_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InstallDate_Fmt", row["InstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TimeOnLoc_Fmt", row["TimeOnLoc_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseDate_Fmt", row["PurchaseDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeWhenPurchased_Fmt", row["AgeWhenPurchased_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONCNTRD", row["LASTRECONCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTCNTRD", row["LASTCERTCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTDATE", row["LASTCERTDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTSITE", row["LASTCERTSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONDATE", row["LASTRECONDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONSITE", row["LASTRECONSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCost_Fmt", row["PurchaseCost_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCondition", row["PurchaseCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CurrentCondition", row["CurrentCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "OwnedBy", row["OwnedBy"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ActualContract", row["ActualContract"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendingMethod", row["VendingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LoadType", row["LoadType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FuelType", row["FuelType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Voltage", row["Voltage"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MeterType", row["MeterType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Sleeved", row["Sleeved"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsWash_Fmt", row["CountAsWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsDry_Fmt", row["CountAsDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceWash_Fmt", row["VendPriceWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceDry_Fmt", row["VendPriceDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CycleTime_Fmt", row["CycleTime_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SpecialPrice_Fmt", row["SpecialPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCycle", row["SuperCycle"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCyclePrice_Fmt", row["SuperCyclePrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Topoff", row["Topoff"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TopoffPrice_Fmt", row["TopoffPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PrevWebId", row["PrevWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "NextWebId", row["NextWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "EquipmentDesc", row["EquipmentDesc"].ToString());
                                sbJson.Append("},");

                            }
                            else
                            {
                                sbJson.Append("{");
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ULN", uln);
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "EquipmentType", row["EquipmentType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "WebId", row["WebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Status", row["Status"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "RoomId", row["RoomId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SerialNumber", row["SerialNumber"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Manufacturer", row["Manufacturer"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Model", row["Model"].ToString().Trim());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ContractType", row["ContractType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "BillingMethod", row["BillingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FirstInstallDate_Fmt", row["FirstInstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeNow_Fmt", row["AgeNow_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "InstallDate_Fmt", row["InstallDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TimeOnLoc_Fmt", row["TimeOnLoc_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseDate_Fmt", row["PurchaseDate_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "AgeWhenPurchased_Fmt", row["AgeWhenPurchased_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONCNTRD", row["LASTRECONCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTCNTRD", row["LASTCERTCNTRD"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTDATE", row["LASTCERTDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTCERTSITE", row["LASTCERTSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONDATE", row["LASTRECONDATE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LASTRECONSITE", row["LASTRECONSITE"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCost_Fmt", row["PurchaseCost_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PurchaseCondition", row["PurchaseCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CurrentCondition", row["CurrentCondition"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "OwnedBy", row["OwnedBy"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "ActualContract", row["ActualContract"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendingMethod", row["VendingMethod"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "LoadType", row["LoadType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "FuelType", row["FuelType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Voltage", row["Voltage"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "MeterType", row["MeterType"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Sleeved", row["Sleeved"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsWash_Fmt", row["CountAsWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CountAsDry_Fmt", row["CountAsDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceWash_Fmt", row["VendPriceWash_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "VendPriceDry_Fmt", row["VendPriceDry_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "CycleTime_Fmt", row["CycleTime_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SpecialPrice_Fmt", row["SpecialPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCycle", row["SuperCycle"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "SuperCyclePrice_Fmt", row["SuperCyclePrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Topoff", row["Topoff"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "TopoffPrice_Fmt", row["TopoffPrice_Fmt"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "PrevWebId", row["PrevWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "NextWebId", row["NextWebId"].ToString());
                                sbJson.AppendFormat("\"{0}\":\"{1}\" ", "EquipmentDesc", row["EquipmentDesc"].ToString());

                                sbJson.Append("},");
                            }
                            sbJson.Replace("},", "}", sbJson.Length - 2, 2);
                        }
                    }

                    DataView dv = ds.Tables[1].DefaultView;
                    dv.Sort = "GPAttributeId";
                    DataTable dtextraattribute = dv.ToTable();

                    if (dtextraattribute.Rows.Count > 0)
                    {
                        sbJson.Append("],");
                        sbJson.Append("\"AttributeList\":[");
                        foreach (DataRow row in dtextraattribute.Rows) // Loop over the rows..
                        {
                            sbJson.Append("{");
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "GPAttributeId", row["GPAttributeId"].ToString());
                            sbJson.AppendFormat("\"{0}\":\"{1}\" ", "AttributeValue", row["AttributeValue"].ToString());
                            sbJson.Append("},");
                        }

                    }
                    sbJson.Replace("},", "}", sbJson.Length - 2, 2);
                    sbJson.Append("]");
                    sbJson.Append("}");

                }
                ds.Dispose();
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "EQMachine", "uln:" + uln + "webid:" + webid);
                return null;
            }
        }


    }
}
       