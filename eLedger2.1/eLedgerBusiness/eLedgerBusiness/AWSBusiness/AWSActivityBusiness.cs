﻿using System;
using System.Data;
using eLedgerBusiness.DAL;
using System.Collections;

namespace eLedgerBusiness
{
    public class AWSActivityBusiness
    {
        public string getActivity(string uln)
        {
            try
            {
                string sb = "";
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);
                //return new WashAWSDAL().getJsonFromQuery("Activity", paramText, paramValues);

                DataTable dtUnsorted = new WashAWSDAL().getDataTable("Activity", paramText, paramValues);

                dtUnsorted = new WashAWSDAL().RenameColumns(dtUnsorted, "GetActivity");

                DataView dv = dtUnsorted.DefaultView;
                dv.Sort = "ActivityDate desc";
                DataTable dt = dv.ToTable();

                return sb = new WashAWSDAL().GetJson(dt);
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "Activity", "ULN"+uln);
                return null;
            }
        }
    }
}