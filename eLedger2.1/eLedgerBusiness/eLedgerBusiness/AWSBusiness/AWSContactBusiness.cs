﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eLedgerEntities;
using System.Data;


using eLedgerBusiness.DAL;
using System.Net;

using System.Collections;
using System.Data.SqlClient;
using System.Text;
using Newtonsoft.Json;

namespace eLedgerBusiness
{
    public class AWSContactBusiness
    {
        public string formatnotetext(string notetext)
        {
            if (notetext.Contains("\\n"))
                notetext = notetext.Replace("\\n", "<br />");
            return notetext;
        }

        public string getContactOverviewJson(string Id, string Type)
        {
            try
            {
                //string sql, sql1, sql3;
                ArrayList paramText, paramValues;// , paramText1, paramText2, paramValues2;
                //sql = "contactSummary";
                //sql1 = "contactMasterCardSummary";

                paramText = new ArrayList();
                paramText.Add("@Id");
                paramValues = new ArrayList();
                paramValues.Add(Id);

                //paramText1 = new ArrayList();
                //paramText2 = new ArrayList();
                //paramText1.Add("@MCID");
                //paramText2.Add("@Id");
                //paramValues = new ArrayList();
                //paramValues2 = new ArrayList();
                //paramValues.Add(Id);
                //paramValues2.Add(Id);

                //sql3 = (Type == "") ? "getRelatedLoc" : ((Type == "Master Payee") ? "getMasterPayeeContactDet_MasterPayee" : "getMasterPayeeContactDet");

                DataSet dscontactOverview = new WashAWSDAL().getAgmtInfoDSRetry("contactOverview", paramText, paramValues, 0);

                //DataTable dtable = new WashAWSDAL().getDataTable(sql, paramText, paramValues);
                DataTable dtable = new WashAWSDAL().RenameColumns(dscontactOverview.Tables[0], "contactSummary");

                //DataTable dt1 = new WashAWSDAL().getDataTable(sql1, paramText1, paramValues);
                DataTable dt1 = new WashAWSDAL().RenameColumns(dscontactOverview.Tables[1], "contactMasterCardSummary");

                DataView dv1 = dt1.DefaultView;
                dv1.Sort = "commentdate_order DESC";
                DataTable dt1Sorted = dv1.ToTable();

                //DataTable dtrelCon = new WashAWSDAL().getDataTable(sql3, paramText2, paramValues2);
                DataTable dtrelCon = new WashAWSDAL().RenameColumns(dscontactOverview.Tables[2], "getRelatedLoc");

                DataView dvrelCon = dtrelCon.DefaultView;
                dvrelCon.Sort = "PropertyStatusOrder,PropertyState,PropertyCity,ULN";
                DataTable dtrelConSorted = dvrelCon.ToTable();

                string Seriliazedatat = "";

                dtable.TableName = "ContactSummary";
                dt1Sorted.TableName = "ContactMaster";
                dtrelConSorted.TableName = "RelatedContacts";
                DataSet ds = new DataSet();



                dtable.Columns.Add("CMSURL", typeof(String));
                dtable.Rows[0]["CMSURL"] = System.Configuration.ConfigurationManager.AppSettings["CMS"] + Id;
                ds.Tables.Add(dtable.Copy());
                ds.Tables.Add(dt1Sorted);
                ds.Tables.Add(dtrelConSorted);

                Seriliazedatat = Newtonsoft.Json.JsonConvert.SerializeObject(ds, Formatting.Indented);

                string Json = Seriliazedatat;
                if (Json.Length > 0)
                    Json = Json.Substring(0, Json.Length - 1);
                Json += ",\"ContactSummaryCount\":\"" + dtable.Rows.Count + "\" " + ",\"ContactMasterCount\":\"" + dt1Sorted.Rows.Count + "\",\"RelatedContactCount\":\"" + dtrelConSorted.Rows.Count + "\"}";


                return Json;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ContactOverview", "Id:" + Id + "Type:" + Type);
                return null;
            }
        }


        public string formattedULN(string ULN)
        {
            if (ULN.Length > 8)
            {
                ULN = ULN.Insert(4, "-");
                ULN = ULN.Insert(7, "-");
                return ULN;
            }
            else
                return ULN;
        }
        public string formattedphone(string phno)
        {
            if (phno.Length > 8)
            {
                phno = phno.Insert(0, "(");
                phno = phno.Insert(4, ") ");
                phno = phno.Insert(9, "-");
                return phno;
            }
            else
                return phno;
        }

        public string getlocContacts(string uln)
        {
            try
            {
                ArrayList paramValues = new ArrayList();
                string queryCode = "getlocContacts";
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);

                DataTable dt = new WashAWSDAL().getDataTableRetry(queryCode, paramText, paramValues);
                dt = new WashAWSDAL().RenameColumns(dt, "getLocContacts");

                DataView dv = dt.DefaultView;
                dv.Sort = "RelationType, ULN";
                DataTable dtSorted = dv.ToTable();

                return Newtonsoft.Json.JsonConvert.SerializeObject(dtSorted, Formatting.Indented);
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "loccontact", "ULN:" + uln);
                return null;
            }
        }

        protected void MakeBlankRow(ref DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
        }
    }
}