﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using eLedgerEntities;
using eLedgerBusiness.DAL;
using System.Net;
using System.Collections;
using System.Data.Odbc;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Hosting;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace eLedgerBusiness.AWSBusiness
{
    public class AWSServiceBusiness
    {
        public string getJsonServiceSummary(string uln)
        {
            try
            {
                int maxDays = 0;
                string serviceSummary = "";
                string serviceCategoryPie = "";
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ULN");
                paramValues.Add(uln);

                DataSet ds = new WashAWSDAL().getAgmtInfoDSRetry("serviceSummary_widget", paramText, paramValues, 1);

                DataTable dt = new WashAWSDAL().RenameColumns(ds.Tables[1], "ServiceSummary");
                serviceSummary = new WashAWSDAL().GetJson(dt);

                DataTable dtServiceCategoryPie = new WashAWSDAL().RenameColumns(ds.Tables[2], "ServiceRecords");
                serviceCategoryPie = ServiceCategoryPie(uln, dtServiceCategoryPie);

                //DataTable dt = new WashAWSDAL().getDataTable("serviceMonths", paramText, paramValues);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow reader in ds.Tables[0].Rows)
                    {
                        if (reader["MaxDays"].ToString().Trim() != "")
                            maxDays = Convert.ToInt32(reader["MaxDays"].ToString());
                        else
                            maxDays = 0;
                    }
                }
                else
                    maxDays = 0;

                //string Json = "\"ServiceSummary\":" + new WashAWSDAL().getJsonFromQuery("serviceSummary", paramText, paramValues) + ",\"maxDays\":\"" + maxDays.ToString() + "\"" + "," + ServiceCategoryPie(uln, "12");
                string Json = "\"ServiceSummary\":" + serviceSummary + ",\"maxDays\":\"" + maxDays.ToString() + "\"" + "," + serviceCategoryPie;
                return Json;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ServiceSummary", "ULN:" + uln);
                return null;
            }
        }

        public DataTable getServiceDetailexport(string serviceCallId)
        {
            try
            {
                DataTable dt = new DataTable();
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ServiceCallId");
                paramValues.Add(serviceCallId);
                dt = new WashAWSDAL().getDataTableRetry("getServicecategory", paramText, paramValues);
                return dt;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ServiceExportTicket", "TicketNo:" + serviceCallId);
                return null;
            }
        }

        public string getJsonServiceDetails(string uln, string dateWindow)
        {
            try
            {
                int rowCountSerRep = 0;
                int rowCountSerM = 0;
                int rowCountSerMM = 0;
                int rowCountSerAll = 0;
                int rowCountSerPC = 0;
                if (dateWindow == "")
                {
                    dateWindow = "90";
                }

                ArrayList paramValues1 = new ArrayList();
                ArrayList paramText1 = new ArrayList();
                paramText1.Add("@ULN");
                paramText1.Add("@Days");
                paramValues1.Add(uln);
                paramValues1.Add(Convert.ToInt32(dateWindow));

                string Seriliazedatat = "";

                DataTable dtUnSorted = new WashAWSDAL().getDataTableRetry("serviceRecords", paramText1, paramValues1);

                dtUnSorted = new WashAWSDAL().RenameColumns(dtUnSorted, "ServiceRecords");

                DataView dv = dtUnSorted.DefaultView;
                dv.Sort = "ServiceCallId DESC";
                DataTable dt = dv.ToTable();

                DataTable dtServiceRepair = new DataTable();
                DataTable dtServiceMaintenance = new DataTable();
                DataTable dtServiceMachMove = new DataTable();
                DataTable dtServicePriceChange = new DataTable();
                DataSet ds = new DataSet();

                if (dt.Rows.Count > 0)
                {
                    DataView datavw = new DataView();
                    datavw = dt.DefaultView;
                    datavw.RowFilter = "RecordType='Service/Repair'";
                    if (datavw.Count > 0)
                    {
                        dtServiceRepair = datavw.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerRep";
                        dtServiceRepair.Columns.Add(newColumn);
                    }
                    DataView datavw1 = new DataView();
                    datavw1 = dt.DefaultView;
                    datavw1.RowFilter = "RecordType='Maintenance'";
                    if (datavw1.Count > 0)
                    {
                        dtServiceMaintenance = datavw1.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerM";
                        dtServiceMaintenance.Columns.Add(newColumn);

                    }
                    DataView datavw2 = new DataView();
                    datavw2 = dt.DefaultView;
                    datavw2.RowFilter = "RecordType='Machine Movement'";
                    if (datavw2.Count > 0)
                    {
                        dtServiceMachMove = datavw2.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerMM";
                        dtServiceMachMove.Columns.Add(newColumn);

                    }
                    DataView datavw3 = new DataView();
                    datavw3 = dt.DefaultView;
                    datavw3.RowFilter = "RecordType='Price Change'";
                    if (datavw3.Count > 0)
                    {
                        dtServicePriceChange = datavw3.ToTable();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                        newColumn.DefaultValue = "SerPc";
                        dtServicePriceChange.Columns.Add(newColumn);

                    }
                    System.Data.DataColumn newColumn1 = new System.Data.DataColumn("ServiceCalltbl", typeof(System.String));
                    newColumn1.DefaultValue = "SerAll";
                    dt.Columns.Add(newColumn1);
                    ds.Tables.Add(dt);
                    ds.Tables.Add(dtServiceRepair);
                    ds.Tables.Add(dtServiceMaintenance);
                    ds.Tables.Add(dtServiceMachMove);
                    ds.Tables.Add(dtServicePriceChange);

                    ds.Tables[0].TableName = "ServiceAll";
                    ds.Tables[1].TableName = "ServiceRepair";
                    ds.Tables[2].TableName = "Maintenance";
                    ds.Tables[3].TableName = "MachineMovement";
                    ds.Tables[4].TableName = "PriceChanges";

                    Seriliazedatat = Newtonsoft.Json.JsonConvert.SerializeObject(ds, Formatting.Indented);

                    rowCountSerAll = ds.Tables[0].Rows.Count;
                    rowCountSerRep = ds.Tables[1].Rows.Count;
                    rowCountSerM = ds.Tables[2].Rows.Count;
                    rowCountSerMM = ds.Tables[3].Rows.Count;
                    rowCountSerPC = ds.Tables[4].Rows.Count;
                }
                ds.Dispose();
                string Json = Seriliazedatat;
                if (Json.Length > 0)
                    Json = Json.Substring(0, Json.Length - 1);
                else
                    Json = "{" + "\"ServiceAll\":\"" + "[]" + "\"" + ",\"ServiceRepair\":\"" + "[]" + "\"" + ",\"Maintenance\":\"" + "[]" + "\"" + ",\"MachineMovement\":\"" + "[]" + "\"" + ",\"PriceChanges\":\"" + "[]" + "\"";
                Json += ",\"PriceChangesTotal\":\"" + rowCountSerPC + "\" " + ",\"MaintenanceTotal\":\"" + rowCountSerM + "\",\"MachineMovementTotal\":\"" + rowCountSerMM + "\"," + "\"ServiceRepairTotal\":\"" + rowCountSerRep + "\"," + "\"ServiceAllTotal\":\"" + rowCountSerAll + "\"" + "}";

                return Json;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ServiceDetails", "uln:" + uln + "dateWindow:" + dateWindow);
                return null;
            }
        }

        public DataSet getexporttoexcel(string uln, string dateWindow)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt;
                DataTable dtlocation;
                DataTable dtservice;
                string query = "serviceRecordsExport";
                ArrayList paramText = new ArrayList();
                ArrayList paramValues = new ArrayList();
                ArrayList paramText2 = new ArrayList();
                ArrayList paramValues2 = new ArrayList();
                paramText.Add("@ULN");
                paramText.Add("@Days");
                string columns = "ServiceCallId As TicketNo,RoomId As Room,CONVERT(VARCHAR(8), CreatedDate_Fmt, 1)As [Created Date],CompletionDate_Fmt As[Closed Date],ResponseHoursInitial_Fmt As [Response (work hrs)],Status,CallType As Type,Description As Complaint,LEFT(ResponseHoursInitial_Fmt, LEN(ResponseHoursInitial_Fmt) - 1) as responsehours";
                paramValues.Add(uln);
                paramValues.Add(Convert.ToInt32(dateWindow));

                paramText2.Add("@ULN");
                paramValues2.Add(uln);
                paramText2.Add("@Mnth");
                paramValues2.Add(Convert.ToInt32("12"));

                DataSet serviceexport = new DataSet();
                ArrayList paramText1 = new ArrayList();
                paramText1.Add("@ULN");
                ArrayList paramValues1 = new ArrayList();
                paramValues1.Add(uln);

                //DataSet ds = new WashAWSDAL().getAgmtInfoDS("serviceSummary_widget", paramText, paramValues);

                //DataTable dt = new WashAWSDAL().RenameColumns(ds.Tables[1], "ServiceSummary");
                //serviceSummary = new WashAWSDAL().GetJson(dt);

                serviceexport = new WashAWSDAL().getdatasetfromColumn(query, paramText, paramValues, columns);

                //dtlocation = new WashAWSDAL().getDataTable("getlocationSummary", paramText1, paramValues1);
                dtlocation = serviceexport.Tables[0];

                DataView dv = serviceexport.Tables[1].DefaultView;
                dv.Sort = "TicketNo DESC ,WebId Desc";
                DataTable dtSorted = dv.ToTable();

                DataView dvdistinct = serviceexport.Tables[1].DefaultView;
                DataTable distinctValues = dvdistinct.ToTable(true, "ticketno", "responsehours");

                decimal avgFiveSbefore = Convert.ToDecimal((distinctValues.Compute("AVG([responsehours])", "")));

                decimal avg = decimal.Round(avgFiveSbefore, 1);

                dtSorted.Columns["ticketno"].ColumnName = "TicketNo";
                dtSorted.Columns["room"].ColumnName = "Room";
                dtSorted.Columns["created_date"].ColumnName = "Created Date";
                dtSorted.Columns["closed_date"].ColumnName = "Closed Date";
                dtSorted.Columns["response_work_hrs"].ColumnName = "Response (work hrs)";
                dtSorted.Columns["status"].ColumnName = "Status";
                dtSorted.Columns["type"].ColumnName = "Type";
                dtSorted.Columns["complaint"].ColumnName = "Complaint";
                dtSorted.Columns["classificationdesc"].ColumnName = "ClassificationDesc";
                dtSorted.Columns["parttype"].ColumnName = "PartType";
                dtSorted.Columns["resolutioncode"].ColumnName = "ResolutionCode";
                dtSorted.Columns["vehicle"].ColumnName = "Vehicle";
                dtSorted.Columns["webid"].ColumnName = "WebId";
                dtSorted.Columns["tech_name"].ColumnName = "Tech Name";

                dtSorted.TableName = "ServiceDetails";

                DataTable dtavgResponse = new DataTable("AvgresponseTime");
                DataColumn avgResponseColumn = dtavgResponse.Columns.Add("AvgResponseTime", typeof(decimal));
                DataRow row = dtavgResponse.NewRow();
                row["AvgResponseTime"] = avg;
                dtavgResponse.Rows.Add(row);

                //dtavgResponse =  serviceexport.Tables[1].TableName = "AvgresponseTime";
                //dtservice = new WashAWSDAL().getDataTable("serviceSummary", paramText1, paramValues1);

                dtservice = serviceexport.Tables[2];
                dtservice = new WashAWSDAL().RenameColumns(dtservice, "ServiceSummary");

                DataTable dtchartUnSorted = serviceexport.Tables[3];
                dtchartUnSorted = new WashAWSDAL().RenameColumns(dtchartUnSorted, "ServiceRecords");

                if (dtchartUnSorted.Select("ServiceRepairCategory = 'Mechanical Related'").Length == 0)
                {
                    DataRow dr = dtchartUnSorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Mechanical Related", "1");
                    dtchartUnSorted.Rows.Add(dr);
                }

                if (dtchartUnSorted.Select("ServiceRepairCategory = 'Building / LR Related'").Length == 0)
                {
                    DataRow dr = dtchartUnSorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Building / LR Related", "2");
                    dtchartUnSorted.Rows.Add(dr);
                }

                if (dtchartUnSorted.Select("ServiceRepairCategory = 'User / Customer Related'").Length == 0)
                {
                    DataRow dr = dtchartUnSorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "User / Customer Related", "3");
                    dtchartUnSorted.Rows.Add(dr);
                }

                if (dtchartUnSorted.Select("ServiceRepairCategory = 'Internal Company Business'").Length == 0)
                {
                    DataRow dr = dtchartUnSorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Internal Company Business", "4");
                    dtchartUnSorted.Rows.Add(dr);
                }

                if (dtchartUnSorted.Select("ServiceRepairCategory = 'Unknown'").Length == 0)
                {
                    DataRow dr = dtchartUnSorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Unknown", "999");
                    dtchartUnSorted.Rows.Add(dr);
                }

                DataView dvchart = dtchartUnSorted.DefaultView;
                dvchart.Sort = "SortOrder,ServiceRepairCategory,ServiceRepairCount,ServiceRepairPercentage";
                DataTable dtchart = dvchart.ToTable();

                dtchart.Columns.Remove("uln");
                dtchart.Columns.Remove("SortOrder");

                ds.Tables.Add(dtlocation.Copy());
                ds.Tables.Add(dtSorted.Copy());
                ds.Tables.Add(dtservice.Copy());
                ds.Tables.Add(dtchart.Copy());
                ds.Tables.Add(dtavgResponse.Copy());
                return ds;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ServiceExport", "ULN:" + uln + "Date:" + dateWindow);
                return null;
            }
        }


        public string getServiceDetail(string serviceCallId, string tblID)
        {
            try
            {
                ArrayList paramValues = new ArrayList();
                ArrayList paramText = new ArrayList();
                paramText.Add("@ServiceCallId");
                paramValues.Add(serviceCallId);
                DataTable dtUnsorted = new WashAWSDAL().getDataTableRetry("serviceDetail", paramText, paramValues);

                dtUnsorted = new WashAWSDAL().RenameColumns(dtUnsorted, "ServiceDetail");

                DataView dv = dtUnsorted.DefaultView;
                dv.Sort = "WebId DESC";
                DataTable dt = dv.ToTable();

                string json = "";
                dt.TableName = "ServiceCallIDDetils";
                if (dt.Rows.Count > 0)
                {
                    json = "{" + "\"ServiceCallIDDetils\":" + Newtonsoft.Json.JsonConvert.SerializeObject(dt, Formatting.Indented) + "}";
                }
                else
                    json = "\"ServiceCallIDDetils\":[]";

                return json;
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ServiceTickno", "serviceCallId:" + serviceCallId + "tblID:" + tblID);
                return null;
            }
        }

        protected DataRow AddNewServiceCategoryRow(DataRow dr, string uln, string serviceRepairCategory, string sortOrder)
        {
            //dr["ULN"] = uln;
            dr["ServiceRepairCategory"] = serviceRepairCategory;
            dr["ServiceRepairCount"] = 0;
            dr["ServiceRepairPercentage"] = 0.00;
            dr["SortOrder"] = sortOrder;
            return dr;
        }

        public string ServiceCategoryPie(string uln, DataTable dtUnsorted)
        {
            try
            {
                //string queryCodeSummary = "serviceCategoryList";
                //int Mnth;
                //if (month == "")
                //    Mnth = 12;
                //else
                //    Mnth = Convert.ToInt32(month);
                //ArrayList paramValues = new ArrayList();
                //ArrayList paramText = new ArrayList();
                //paramText.Add("@ULN");
                //paramValues.Add(uln);
                //paramText.Add("@Mnth");
                //paramValues.Add(Mnth);
                int Totalmachincerepair = 0;
                //DataTable dtUnsorted = new WashAWSDAL().getDataTable(queryCodeSummary, paramText, paramValues);
                //dtUnsorted = new WashAWSDAL().RenameColumns(dtUnsorted, "ServiceRecords");

                if(dtUnsorted.Select("ServiceRepairCategory = 'Mechanical Related'").Length == 0)
                {
                    DataRow dr = dtUnsorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Mechanical Related", "1");
                    dtUnsorted.Rows.Add(dr);
                }

                if (dtUnsorted.Select("ServiceRepairCategory = 'Building / LR Related'").Length == 0)
                {
                    DataRow dr = dtUnsorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Building / LR Related", "2");
                    dtUnsorted.Rows.Add(dr);
                }

                if (dtUnsorted.Select("ServiceRepairCategory = 'User / Customer Related'").Length == 0)
                {
                    DataRow dr = dtUnsorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "User / Customer Related", "3");
                    dtUnsorted.Rows.Add(dr);
                }

                if (dtUnsorted.Select("ServiceRepairCategory = 'Internal Company Business'").Length == 0)
                {
                    DataRow dr = dtUnsorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Internal Company Business", "4");
                    dtUnsorted.Rows.Add(dr);
                }

                if (dtUnsorted.Select("ServiceRepairCategory = 'Unknown'").Length == 0)
                {
                    DataRow dr = dtUnsorted.NewRow();
                    dr = AddNewServiceCategoryRow(dr, uln, "Unknown", "999");
                    dtUnsorted.Rows.Add(dr);
                }

                DataView dv = dtUnsorted.DefaultView;
                dv.Sort = "SortOrder,ServiceRepairCategory,ServiceRepairCount,ServiceRepairPercentage";
                DataTable dt = dv.ToTable();

                StringBuilder sbJson = new StringBuilder();
                sbJson.Append("\"ServiceCategories\":[{ ");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["ServiceRepairCategory"].ToString().Trim() == "Building / LR Related")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Building_Issue", "Building / LR Issues");
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Building_Issue_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Building_Issue_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "User / Customer Related")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Customer_Generated_Repair", "Customer Education Tickets");
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Customer_Generated_Repair_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\",  ", "Customer_Generated_Repair_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "Internal Company Business")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Internal_Company_Business", dr["ServiceRepairCategory"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Internal_Company_Business_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Internal_Company_Business_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "Mechanical Related")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Mechanical_Repair", "Machine Repairs");
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Mechanical_Repair_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Mechanical_Repair_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    else if (dr["ServiceRepairCategory"].ToString().Trim() == "Unknown")
                    {
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Unknown", dr["ServiceRepairCategory"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Unknown_Count", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Unknown_Percentage", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString() + "%");
                    }
                    Totalmachincerepair = Totalmachincerepair + Convert.ToInt32(dr["ServiceRepairCount"]);
                }

                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Total_Machine", Totalmachincerepair);
                sbJson.AppendFormat("\"{0}\":\"{1}\", ", "Total_Machine_Percentage", "100%");


                if (sbJson.ToString() != "\"ServiceCategories\":[{ ")
                {
                    sbJson.Replace(", ", "", sbJson.Length - 2, 2);
                    sbJson.Append("}],");
                }
                else
                {
                    sbJson.Remove(sbJson.Length - 2, 2);
                    sbJson.Append("],");
                }

                sbJson.Append("\"Servicecategorypie\":[");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["ServiceRepairCount"].ToString().Trim() != "0")
                    {
                        sbJson.Append("{");
                        if (dr["ServiceRepairCategory"].ToString().Trim() == "Mechanical Related")
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", "Machine Repairs");
                        else if (dr["ServiceRepairCategory"].ToString().Trim() == "User / Customer Related")
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", "Customer Education Tickets");
                        else if (dr["ServiceRepairCategory"].ToString().Trim() == "Building / LR Related")
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", "Building / LR Issues");
                        else
                            sbJson.AppendFormat("\"{0}\":\"{1}\", ", "category", dr["ServiceRepairCategory"].ToString().Trim());
                        sbJson.AppendFormat("\"{0}\":\"{1}\", ", "value", dr["ServiceRepairCount"]);
                        sbJson.AppendFormat("\"{0}\":\"{1}\" ", "series1", Math.Round(Convert.ToDecimal(dr["ServiceRepairPercentage"])).ToString());
                        sbJson.Append("}, ");
                    }
                }
                sbJson.Replace(", ", "", sbJson.Length - 2, 2);
                sbJson.Append("]");
                return sbJson.ToString();
            }
            catch (Exception ex)
            {
                new WashAWSDAL().CreateLog(ex.ToString(), "ServiceChart", "uln:" + uln + "month:" + "12");
                return null;
            }
        }

    }
}