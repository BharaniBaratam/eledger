﻿using eLedgerBusiness.DAL;
using eLedgerEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using eLedgerBusiness.DAL;
using System.Data.SqlClient;
using Newtonsoft.Json;
using eLedgerBusiness.Utils;
namespace eLedgerBusiness
{
    public class AWSSearchBusiness
    {
        public string SearchLocation(Location searchParam)
        {
            string querycode;
            ArrayList paramText = new ArrayList();
            //paramText.Add("@SearchULN");
            //paramText.Add("@SearchName");
            //paramText.Add("@LaundryRoomDesc");
            //paramText.Add("@SearchAddress");
            //paramText.Add("@SearchCity");
            //paramText.Add("@SearchState");
            //paramText.Add("@SearchZip");
            //paramText.Add("@SearchWebNum");

            paramText.Add("@SearchULN");
            paramText.Add("@SearchText");
            paramText.Add("@SearchWebNum");

            string SearchWebNum = searchParam.WebNum.ToString().Trim();
            if (SearchWebNum == "")
            {
                querycode = "getLocationsListJson";
            }
            else
            {
                querycode = "getLocationsListJson2";
            }
            ArrayList paramValues = new ArrayList();
            string search_text = "";
            string Zip1 = searchParam.Zip.ToString().Replace("%20", " ");
            string ULN1 = searchParam.ULN.ToString().Replace("%20", " ");

            search_text = "~^"
                //+ eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(HttpUtility.UrlDecode(searchParam.ULN.ToString()))).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(ULN1)).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Name)).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Address)).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.City)).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.State)).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                //+ eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(HttpUtility.UrlDecode(searchParam.Zip.ToString()))).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"                
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(Zip1)).TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^"
                + eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.LaundryRooms_Fmt)).TrimStart().TrimEnd().ToUpper().Replace(" ", "").Replace("-", "").Replace("`", "") + "%";

            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(HttpUtility.UrlDecode(searchParam.ULN.ToString()))).TrimEnd().ToUpper());
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(ULN1)).TrimStart().TrimEnd().ToUpper());
            paramValues.Add(search_text);
            paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.WebNum.Replace("`", "").Replace("%", "*"))).TrimStart().TrimEnd().ToUpper());
            
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(HttpUtility.UrlDecode(searchParam.ULN.ToString()))).TrimEnd().ToUpper());
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Name)).TrimEnd().ToUpper());
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.LaundryRooms_Fmt)).TrimEnd().ToUpper().Replace(" ", "").Replace("-", ""));
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.Address)).TrimEnd().ToUpper());
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.City)).TrimEnd().ToUpper());
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.State)).TrimEnd().ToUpper());
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(HttpUtility.UrlDecode(searchParam.Zip.ToString()))).TrimEnd().ToUpper());
            //paramValues.Add(eLedgerBusiness.Utils.Strings.WildCardAdjust(replaceNull(searchParam.WebNum)).TrimEnd().ToUpper());
            //string columns = "ULN, Address, City, State, Zip, Status, ContractType, PropertyName";

            DataTable dt = new WashAWSDAL().getDataTableRetry(querycode, paramText, paramValues);

            dt = new WashAWSDAL().RenameColumns(dt, "GetLocationsListJson");

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName == "ULN")
                        if (dr[col].ToString().Length > 8)
                        {
                            row.Add(col.ColumnName, dr[col].ToString().Substring(0, 4) + "-" + dr[col].ToString().Substring(4, 2) + "-" + dr[col].ToString().Substring(6, 3));
                        }
                        else
                        {
                            row.Add(col.ColumnName.Trim(), dr[col]);
                        }
                    else
                        row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            string jsonOutput = serializer.Serialize(rows); ;
            if (jsonOutput.ToString().Length > 0)
                jsonOutput = "{\"SearchLocation\":" + jsonOutput + ",\"total\":\"" + dt.Rows.Count + "\"" + "}";

            //string jsondata = "{\"SearchLocation\":" + jsonOutput + "}";
            return jsonOutput;
        }

        public string SearchContactList(string id, string name, string address, string city, string state, string zip, string phone, string flag)
        {
            DataSet ds = new DataSet("Data");
            DataTable dt;
            string searchName = new WashDAL().MakeBlkstring(name).TrimStart().TrimEnd();
            string searchId;
            string contactList;
            if ((id == "") || (id == null))
                searchId = new WashDAL().MakeBlkstring(id).TrimStart().TrimEnd();
            else
                // searchId = "*" + new WashDAL().MakeBlkstring(id);
                searchId = new WashDAL().MakeBlkstring(id).TrimStart().TrimEnd();
            string searchAddress = new WashDAL().MakeBlkstring(address).TrimStart().TrimEnd();
            string searchCity = new WashDAL().MakeBlkstring(city).TrimStart().TrimEnd();
            string searchState = new WashDAL().MakeBlkstring(state).TrimStart().TrimEnd();
            string searchZip = new WashDAL().MakeBlkstring(zip).TrimStart().TrimEnd();
            string searchPhone = new WashDAL().MakeBlkstring(phone).TrimStart().TrimEnd();
            string searchFlag = new WashDAL().MakeBlkstring(flag).TrimStart().TrimEnd();
            int maxSearchResults = eLedgerBusiness.Utils.ReadWebConfig.MaxSearchResults();
            string escClause = "ESCAPE '" + eLedgerBusiness.Utils.ReadWebConfig.EscapeCode() + "'";
            //string querycode = "getContactlist";
            if (searchId != "")
                searchId = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchId);

            string MCID, contactID, startSearchId_MC, startSearchId_M, searchNewId, s;
            int CtIDLen;
            CtIDLen = searchId.Length;

            searchName = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchName);
            searchAddress = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchAddress);
            searchCity = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchCity);
            searchState = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchState);
            searchZip = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchZip);
            searchPhone = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchPhone.Trim().Replace("-", ""));
            searchFlag = eLedgerBusiness.Utils.Strings.WildCardAdjust(searchFlag);

            if (searchId.Length >= 2)
            {
                startSearchId_MC = searchId.Substring(0, 2);//'MC'
                startSearchId_M = searchId.Substring(0, 1);//'M'
            }
            else
            {
                startSearchId_MC = searchId;
                startSearchId_M = searchId;
            }
            //type 0 condition when flag == "0" 
            if (flag == "0")// && searchId != ""
            {
                if (startSearchId_MC.ToUpper() == "MC")
                {
                    if (searchId.Length >= 2)
                    {
                        MCID = "0000000" + searchId.Substring(2);
                        MCID = "MC" + MCID.Substring(MCID.Length - 7);
                    }
                    else
                    {
                        MCID = searchId;
                    }
                }
                else
                {
                    MCID = searchId;
                }
                contactList = searchContacts("getContactlist_Type0", searchId, MCID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                return contactList.ToString();
            }

            else if (flag == "1")
            {
                if (startSearchId_MC.ToUpper() == "MC")
                {
                    MCID = "0000000" + searchId.Substring(2);
                    contactID = "MC" + MCID.Substring(MCID.Length - 7);
                    contactList = searchContacts("getContactlist_Type1", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
                else if (startSearchId_M.ToUpper() == "M")
                {
                    MCID = "0000000" + searchId.Substring(1);
                    contactID = "MC" + MCID.Substring(MCID.Length - 7);
                    contactList = searchContacts("getContactlist_Type1", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
                else if (flag == "1" && searchId != "")
                {
                    searchNewId = "0000000" + searchId;
                    contactID = "MC" + searchNewId.Substring(searchNewId.Length - 7);
                    contactList = searchContacts("getContactlist_Type1", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
                else
                {
                    contactID = searchId;
                    contactList = searchContacts("getContactlist_Type1", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
            }
            else if (flag == "5")
            {
                if (startSearchId_MC.ToUpper() == "MC")
                {
                    MCID = "0000000" + searchId.Substring(2);
                    contactID = "MC" + MCID.Substring(MCID.Length - 7);
                    contactList = searchContacts("getContactlist_Type5", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
                else if (startSearchId_M.ToUpper() == "M")
                {
                    MCID = "0000000" + searchId.Substring(1);
                    contactID = "MC" + MCID.Substring(MCID.Length - 7);
                    contactList = searchContacts("getContactlist_Type5", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
                else if (flag == "1" && searchId != "")
                {
                    searchNewId = "0000000" + searchId;
                    contactID = searchNewId.Substring(searchNewId.Length - 7);
                    contactList = searchContacts("getContactlist_Type5", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
                else
                {
                    contactID = searchId;
                    contactList = searchContacts("getContactlist_Type5", "", contactID, searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                    return contactList.ToString();
                }
            }
            ///type else condition when flag != "5" && flag != "1" && flag != "0")
            else if (flag != "5" && flag != "1" && flag != "0")
            {
                contactList = searchContacts("getContactlist_else", searchId, "", searchName, searchAddress, searchCity, searchState, searchZip, searchPhone, searchFlag);
                return contactList.ToString();
            }

            else
            {
                contactList = "";
                return contactList.ToString();
            }


        }


        public string searchContacts(string querycode, string id, string contactID_MCID, string name, string address, string city, string state, string zip, string phone, string flag)
        {


            string search_text = "";
            if (querycode == "getContactlist_else")
            {
                search_text = "~^" + name.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + id.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + address.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + city.TrimStart().TrimEnd().ToUpper().Replace("`", "")
                    + "%~^" + state.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + zip.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + phone.TrimStart().TrimEnd().ToUpper().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("`", "") + "%";
            }
            else if (querycode == "getContactlist_Type1")
            {
                search_text = "~^" + name.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + contactID_MCID.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + address.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + city.TrimStart().TrimEnd().ToUpper().Replace("`", "")
                    + "%~^" + state.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + zip.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + phone.TrimStart().TrimEnd().ToUpper().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("`", "") + "%";
            }
            else if (querycode == "getContactlist_Type5")
            {
                search_text = "~^" + name.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + contactID_MCID.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + address.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + city.TrimStart().TrimEnd().ToUpper().Replace("`", "")
                    + "%~^" + state.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + zip.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + phone.TrimStart().TrimEnd().ToUpper().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("`", "") + "%";
            }
            else if (querycode == "getContactlist_Type0")
            {
                string contactId = (id.TrimStart().TrimEnd().ToUpper() != "") ? id.TrimStart().TrimEnd().ToUpper() : contactID_MCID.TrimStart().TrimEnd().ToUpper();
                search_text = "~^" + name.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + contactId.Replace("`", "") + "%~^" + address.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + city.TrimStart().TrimEnd().ToUpper().Replace("`", "")
                    + "%~^" + state.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + zip.TrimStart().TrimEnd().ToUpper().Replace("`", "") + "%~^" + phone.TrimStart().TrimEnd().ToUpper().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("`", "") + "%";
            }

            ArrayList paramText = new ArrayList();
            paramText.Add("@SearchText");

            //paramText.Add("@SearchId");
            //paramText.Add("@contactID_MCID");
            //paramText.Add("@SearchName");
            //paramText.Add("@SearchAddress");
            //paramText.Add("@SearchCity");
            //paramText.Add("@SearchState");
            //paramText.Add("@SearchZip");
            //paramText.Add("@SearchPhone");
            //paramText.Add("@Flag");

            ArrayList paramValues = new ArrayList();
            paramValues.Add(search_text);

            //paramValues.Add(id);
            //paramValues.Add(contactID_MCID);
            //paramValues.Add(name);
            //paramValues.Add(address);
            //paramValues.Add(city);
            //paramValues.Add(state);
            //paramValues.Add(zip);
            //paramValues.Add(phone);
            //paramValues.Add(flag);

            string Json = "";
            try
            {

                DataTable dt = new WashAWSDAL().getDataTableRetry(querycode, paramText, paramValues);
                dt = new WashAWSDAL().RenameColumns(dt, "getContactlist");


                //DataView dv = dt.DefaultView;
                //dv.Sort = "Name";
                //DataTable dtSorted = dv.ToTable();

                Json = Newtonsoft.Json.JsonConvert.SerializeObject(dt, Formatting.Indented);

                Json = "{\"SearchContact\":" + Json + ",\"total\":\"" + dt.Rows.Count + "\" " + "}";

            }
            catch
            {
                Json = "{\"SearchContact\":" + "[]" + ",\"total\":\"" + "[]" + "\"}";
            }
            return Json;
        }


        public string replaceNull(string value)
        {
            if (value == null)
                return "";
            else
                return value;
        }
    }
}