﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  eLedgerBusiness.Utils
{
    public class ReadWebConfig
    {
        /// <summary>
        /// Read variables from the Web.config file
        /// </summary>
        public ReadWebConfig()
        {
        }

        /// <summary>
        /// Gets the connection string to MSSQL 
        /// </summary>
        public static string Get_SQL_Conn()
        {
            if (HttpRuntime.Cache["SQL_Conn"] == null)
                HttpRuntime.Cache["SQL_Conn"] = ReturnValue("SQL_Conn");
            return HttpRuntime.Cache["SQL_Conn"].ToString();
        }

        /// <summary>
        /// Root path based on c: up to the root of this project
        /// </summary>
        public static string GetRootPath()
        {
            if (HttpRuntime.Cache["rootPath"] == null)
                HttpRuntime.Cache["rootPath"] = ReturnValue("rootPath");
            return HttpRuntime.Cache["rootPath"].ToString();
        }

        /// <summary>
        /// HTTP Root path based on http:// up to the root of this project
        /// </summary>
        public static string GethttpRootPath()
        {
            if (HttpRuntime.Cache["httpRootPath"] == null)
                HttpRuntime.Cache["httpRootPath"] = ReturnValue("httpRootPath");
            return HttpRuntime.Cache["httpRootPath"].ToString();
        }

        /// <summary>
        /// XSL files path based on c: up to the XslFiles folder of this project
        /// </summary>
        public static string GetXslPath()
        {
            if (HttpRuntime.Cache["xslPath"] == null)
                HttpRuntime.Cache["xslPath"] = ReturnValue("xslPath");
            return GetRootPath() + HttpRuntime.Cache["xslPath"].ToString();
        }

        /// <summary>
        /// XML files path based on c: up to the XmlFiles folder of this project
        /// </summary>
        public static string GetXmlPath()
        {
            if (HttpRuntime.Cache["xmlPath"] == null)
                HttpRuntime.Cache["xmlPath"] = ReturnValue("xmlPath");
            return GetRootPath() + HttpRuntime.Cache["xmlPath"].ToString();
        }

        /// <summary>
        /// File path to the SiteMaps physical directory
        /// </summary>
        public static string GetSiteMapsPath()
        {
            if (HttpRuntime.Cache["SiteMapsPath"] == null)
                HttpRuntime.Cache["SiteMapsPath"] = ReturnValue("SiteMapsPath");
            return HttpRuntime.Cache["SiteMapsPath"].ToString();
        }

        /// <summary>
        /// HTTP path to the SiteMaps virtual directory
        /// </summary>
        public static string GetSiteMapsURL()
        {
            if (HttpRuntime.Cache["SiteMapsURL"] == null)
                HttpRuntime.Cache["SiteMapsURL"] = ReturnValue("SiteMapsURL");
            return HttpRuntime.Cache["SiteMapsURL"].ToString();
        }

        /// <summary>
        /// Get the row ID of the 'Default' role
        /// </summary>
        public static int GetDefaultRole()
        {
            if (HttpRuntime.Cache["DefaultRole"] == null)
                HttpRuntime.Cache["DefaultRole"] = ReturnValue("DefaultRole");
            return Convert.ToInt32(HttpRuntime.Cache["DefaultRole"].ToString());
        }

        /// <summary>
        /// Comma delimited string of RoleIDs that are allowed to see Route Day. e.g. "10000003,10000004"
        /// </summary>
        public static string GetShowRouteDayRoleList()
        {
            if (HttpRuntime.Cache["ShowRouteDayRoleList"] == null)
                HttpRuntime.Cache["ShowRouteDayRoleList"] = ReturnValue("ShowRouteDayRoleList");
            return HttpRuntime.Cache["ShowRouteDayRoleList"].ToString();
        }

        /// <summary>
        /// Get the AllowAllUsers bool.  
        /// If true, any non-anonymous user that is not in 
        /// the Sys_User table gets all the default pages.  
        /// If false, only users listed in the Sys_User table 
        /// can use this application
        /// </summary>
        /// <returns>True/False, can unregistered viewers see pages</returns>
        public static bool AllowAllUsers()
        {
            if (HttpRuntime.Cache["AllowAllUsers"] == null)
                HttpRuntime.Cache["AllowAllUsers"] = ReturnValue("AllowAllUsers");
            return Convert.ToBoolean(HttpRuntime.Cache["AllowAllUsers"].ToString());
        }

        /// <summary>
        /// Default page for errors
        /// </summary>
        /// <returns>Relative file path to page</returns>
        public static string ErrorDefaultPage()
        {
            if (HttpRuntime.Cache["ErrorDefaultPage"] == null)
                HttpRuntime.Cache["ErrorDefaultPage"] = ReturnValue("ErrorDefaultPage");
            return HttpRuntime.Cache["ErrorDefaultPage"].ToString();
        }

        /// <summary>
        /// Default CMS link
        /// </summary>
        /// <returns>Absolute/Full path to GMS</returns>
        public static string CMS()
        {
            if (HttpRuntime.Cache["CMS"] == null)
                HttpRuntime.Cache["CMS"] = ReturnValue("CMS");
            return HttpRuntime.Cache["CMS"].ToString();
        }

        /// <summary>
        /// Default Location Inventory link
        /// </summary>
        /// <returns>Absolute/Full path to Location Inventory web page</returns>
        public static string LocInventory()
        {
            if (HttpRuntime.Cache["LocInventory"] == null)
                HttpRuntime.Cache["LocInventory"] = ReturnValue("LocInventory");
            return HttpRuntime.Cache["LocInventory"].ToString();
        }

        /// <summary>
        /// Default View MMR link
        /// </summary>
        /// <returns>Absolute/Full path to Location Inventory web page</returns>
        public static string ViewMMR()
        {
            if (HttpRuntime.Cache["ViewMMR"] == null)
                HttpRuntime.Cache["ViewMMR"] = ReturnValue("ViewMMR");
            return HttpRuntime.Cache["ViewMMR"].ToString();
        }

        /// <summary>
        /// Default View Refund link
        /// </summary>
        /// <returns>Absolute/Full path to View Refund web page</returns>
        public static string ViewRefund()
        {
            if (HttpRuntime.Cache["ViewRefund"] == null)
                HttpRuntime.Cache["ViewRefund"] = ReturnValue("ViewRefund");
            return HttpRuntime.Cache["ViewRefund"].ToString();
        }

        /// <summary>
        /// Default AS400 historical ledger folder path. Each file saved as ULN.gif
        /// </summary>
        /// <returns>Absolute/Full path to as/400 pdf ledger pages</returns>
        public static string AS400LedgersPath()
        {
            if (HttpRuntime.Cache["AS400LedgersPath"] == null)
                HttpRuntime.Cache["AS400LedgersPath"] = ReturnValue("AS400LedgersPath");
            return HttpRuntime.Cache["AS400LedgersPath"].ToString();
        }

        /// <summary>
        /// Default page for users that do not match any roles, or do 
        /// not have a home page assigned.
        /// </summary>
        /// <returns>Relative file path to page</returns>
        public static string NoAuthDefaultPage()
        {
            if (HttpRuntime.Cache["NoAuthDefaultPage"] == null)
                HttpRuntime.Cache["NoAuthDefaultPage"] = ReturnValue("NoAuthDefaultPage");
            return HttpRuntime.Cache["NoAuthDefaultPage"].ToString();
        }

        /// <summary>
        /// Role id for no authorization
        /// </summary>
        /// <returns>ID of Role for non-auth users</returns>
        public static int NoAuthRoleId()
        {
            if (HttpRuntime.Cache["NoAuthRoleId"] == null)
                HttpRuntime.Cache["NoAuthRoleId"] = ReturnValue("NoAuthRoleId");
            return Convert.ToInt32(HttpRuntime.Cache["NoAuthRoleId"].ToString());
        }

        /// <summary>
        /// Get the number of days (double) that a user is allowed to delete their notes. 
        /// </summary>
        /// <returns>(double)Number of days</returns>
        public static int DaysAllowedToDeleteNote()
        {
            int days;
            try
            {
                if (HttpRuntime.Cache["DaysAllowedToDeleteNote"] == null)
                    HttpRuntime.Cache["DaysAllowedToDeleteNote"] = ReturnValue("DaysAllowedToDeleteNote");
                days = Convert.ToInt32(HttpRuntime.Cache["DaysAllowedToDeleteNote"].ToString());
            }
            catch (Exception)
            {
                days = 0;
            }

            return days;
        }

        /// <summary>
        /// Get the number of menu buttons that go on a row 
        /// before it wraps to the next row
        /// </summary>
        /// <returns>Number of menu buttons per row</returns>
        public static int MenuBtnsPerRow()
        {
            if (HttpRuntime.Cache["MenuBtnsPerRow"] == null)
                HttpRuntime.Cache["MenuBtnsPerRow"] = ReturnValue("MenuBtnsPerRow");
            return Convert.ToInt32(HttpRuntime.Cache["MenuBtnsPerRow"].ToString());
        }

        /// <summary>
        /// Path to do a TD Servlet request
        /// </summary>
        /// <returns>URL of the TD Engine Servlet</returns>
        public static string TDServletPath()
        {
            if (HttpRuntime.Cache["TDServletPath"] == null)
                HttpRuntime.Cache["TDServletPath"] = ReturnValue("TDServletPath");
            return HttpRuntime.Cache["TDServletPath"].ToString();
        }

        /// <summary>
        /// Path to Alerts Documentation
        /// </summary>
        /// <returns>URL of the TD Engine Servlet</returns>
        public static string AlertsHelpPath()
        {
            if (HttpRuntime.Cache["AlertsHelp"] == null)
                HttpRuntime.Cache["AlertsHelp"] = ReturnValue("AlertsHelp");
            return HttpRuntime.Cache["AlertsHelp"].ToString();
        }

        /// <summary>
        /// Height of a TD Map
        /// </summary>
        /// <returns>URL of the TD Engine Servlet</returns>
        public static string MapHeight()
        {
            if (HttpRuntime.Cache["MapHeight"] == null)
                HttpRuntime.Cache["MapHeight"] = ReturnValue("MapHeight");
            return HttpRuntime.Cache["MapHeight"].ToString();
        }

        /// <summary>
        /// Width of a TD Map
        /// </summary>
        /// <returns>URL of the TD Engine Servlet</returns>
        public static string MapWidth()
        {
            if (HttpRuntime.Cache["MapWidth"] == null)
                HttpRuntime.Cache["MapWidth"] = ReturnValue("MapWidth");
            return HttpRuntime.Cache["MapWidth"].ToString();
        }

        /// <summary>
        /// Escape char to use when doing 'like queries
        /// </summary>
        /// <returns>Char used to escaping</returns>
        public static string EscapeCode()
        {
            if (HttpRuntime.Cache["EscapeCode"] == null)
                HttpRuntime.Cache["EscapeCode"] = ReturnValue("EscapeCode");
            return HttpRuntime.Cache["EscapeCode"].ToString();
        }

        /// <summary>
        /// Get the maximum number of records possible to 
        /// return from a query - this is a limiter for performance
        /// </summary>
        /// <returns>
        /// maximum number of records possible to 
        /// return from a query
        /// </returns>
        public static int MaxSearchResults()
        {
            if (HttpRuntime.Cache["MaxSearchResults"] == null)
                HttpRuntime.Cache["MaxSearchResults"] = ReturnValue("MaxSearchResults");
            return Convert.ToInt32(HttpRuntime.Cache["MaxSearchResults"].ToString());
        }

        /// <summary>
        /// return ASAP Agreement info Web service URI
        /// </summary>
        /// <returns>
        /// ASAP Agreement info Web service URI        
        /// </returns>
        public static string ASAPWSURI()
        {
            if (HttpRuntime.Cache["ASAP"] == null)
                HttpRuntime.Cache["ASAP"] = ReturnValue("ASAP");
            return HttpRuntime.Cache["ASAP"].ToString();
        }

        /// <summary>
        /// Get the Google map key based on a host name
        /// </summary>
        /// <param name="hostName">Host name of domain (i.e. dot_net)</param>
        /// <returns>Char used to escaping</returns>
        public static string GoogleMapKey(string hostName)
        {
            if (HttpRuntime.Cache["GoogleMapKey"] == null)
                HttpRuntime.Cache["GoogleMapKey"] = ReturnValue("GoogleMapKey_" + hostName);
            return HttpRuntime.Cache["GoogleMapKey"].ToString();
        }

        /// <summary>
        /// Do all the work to get the web config variable
        /// </summary>
        /// <param name="key">key of the Web.config variable</param>
        /// <returns>Value associated to the key in the Web.config</returns>
        private static string ReturnValue(string key)
        {
            string[] webconfig = System.Configuration.ConfigurationManager.AppSettings.GetValues(key);
            string val = webconfig[0];
            return val;
        }
    }
}
