﻿using eLedgerBusiness;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Drawing;
using eLedgerBusiness.DAL;

namespace eLedgerMVC.Controllers
{
    public class AS400PDFPageController : Controller
    {            
        [HttpGet]
        
        public ActionResult getas400pdf(string uln)
        {
            byte[] data;            
            using (WebClient wc = new WebClient())
            {
                wc.Credentials = CredentialCache.DefaultCredentials;
                string path = "";
                Stream strm = null;
                try
                {
                    try
                    {
                        path = new AS400PDFPageBusiness().getas400pdfdetails(uln);
                        strm = wc.OpenRead(path);
                        data = new byte[(int)strm.Length];
                        strm.Read(data, 0, data.Length);
                    }
                    catch
                    {
                        path = eLedgerBusiness.Utils.ReadWebConfig.AS400LedgersPath() + "NoLedger.gif";
                        strm = wc.OpenRead(path);
                        data = new byte[(int)strm.Length];
                        strm.Read(data, 0, data.Length);
                    }
                }
                catch(Exception ex)
                {
                    new WashDAL().CreateLog(ex.ToString(), "AS400", "ULN:" + uln); 
                    var result = new FilePathResult("~/Views/Shared/ServerError.html", "text/html");
                    return result;
                }
                strm.Close();
            }
            return Content(Convert.ToBase64String(data));
        }
    }
}
