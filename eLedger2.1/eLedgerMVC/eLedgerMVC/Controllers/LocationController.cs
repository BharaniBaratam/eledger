﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web.Mvc;
using System.Web.UI;
using System.Net.Http.Headers;
using eLedgerBusiness.AWSBusiness;

namespace eLedgerMVC.Controllers
{
    public class LocationController : ApiController
    {
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]         
        public dynamic SearchLocation(Location searchParam)
        {
            string SearchLocation = new AWSSearchBusiness().SearchLocation(searchParam);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(SearchLocation, System.Text.Encoding.UTF8, "application/json")
            };
            return response;

        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]         
        public dynamic getLocationSummary(string ULN)
        {
            string Location = "{" + new AWSLocationBusiness().getLocationSummaryJson(ULN) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Location, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }         
    }
}
