﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;

namespace eLedgerMVC.Controllers
{
    public class LegalController : ApiController
    {
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getLegal(string ULN)
        {
            string Legal = new LegalBusiness().LegalIssues(ULN);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(Legal, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic addLegalNotes(string uln, string noteText, string visibility, string status, string threadid)
        {
            string result = new LegalBusiness().AddNewLegalNote(uln, noteText, visibility, status, threadid);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(result, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic getLegalDetbythreadid(string uln, string id)
        {
            string json = "{\"LegalInner\":" + new LegalBusiness().getlegalissuebyid(uln, id) + "}";
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(json, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic deleteLegalNotesById(string uln, string id, string threadid)
        {
            string result = new LegalBusiness().deleteLegalNotesById(uln, id, threadid);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(result, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
         public dynamic deleteAllLegalNotes(string uln,string threadid)
         {
             string result = new LegalBusiness().deleteAllLegalNotes(uln,threadid);
             var response = new HttpResponseMessage(HttpStatusCode.OK)
             {
                 Content = new StringContent(result, System.Text.Encoding.UTF8, "application/json")
             };
             return response;
         }

    }
}
