﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using eLedgerEntities;
using eLedgerBusiness;
using System.Web;
using System.Web.Mvc;

namespace eLedgerMVC.Controllers
{
    public class NotesController : ApiController
    {
          [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic addNotesDetail(string ULN, string notetext)
        {
            string note = new NotesBusiness().AddNewNote(ULN,notetext) ;
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(note, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
          [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
        public dynamic deleteNotes(string uln, string id, string notetext)
        {
            string list = new NotesBusiness().updatedeleteNotes(uln, id, notetext);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(list, System.Text.Encoding.UTF8, "application/json")
            };
            return response;
        }
          [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
         public dynamic validateuln(string id)
         {
             //here ULN is ID.
             string jsondata = new NotesBusiness().validateId(id);
             var response = new HttpResponseMessage(HttpStatusCode.OK)
             {
                 Content = new StringContent(jsondata, System.Text.Encoding.UTF8, "application/json")
             };
             return response;
         }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
         public dynamic updatedeleteNotes(string uln, string id,string notetext)
         {             
             notetext = HttpUtility.UrlDecode(notetext);
             string list = new NotesBusiness().SubmitUpdate(uln, id, notetext,"","");
             var response = new HttpResponseMessage(HttpStatusCode.OK)
             {
                 Content = new StringContent(list, System.Text.Encoding.UTF8, "application/json")
             };
             return response;
         }
         [System.Web.Http.HttpPost, System.Web.Http.HttpGet]
         public dynamic getNotes(string ULN)
         {
             string Notes = "{\"Notes\":" + new NotesBusiness().getJsonNotes(ULN) + "}";
             var response = new HttpResponseMessage(HttpStatusCode.OK)
             {
                 Content = new StringContent(Notes, System.Text.Encoding.UTF8, "application/json")
             };
             return response;
         }
       
       
        

    }
}
