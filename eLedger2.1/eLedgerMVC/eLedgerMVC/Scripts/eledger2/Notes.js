﻿var tmpNote;
var noteappPath = location.pathname;
if (noteappPath == "/") noteappPath = "";
if (noteappPath.indexOf("/Start") >= 0) {
    noteappPath = noteappPath.substring(0, noteappPath.indexOf("/Start"))
}
noteappPath = "";
var noteUrl = noteappPath + '/api/Front';


function Notes_Load() {
    var sPageURL = window.location.pathname;
    var sParameterName = sPageURL.split('/');
    var uln = sParameterName[4];
    var Id = sParameterName[5];
    $('#hd_Uln').val(uln);
    if (Id == 0) {
        $('#txt_NoteText').val("");
        var a = document.getElementById('bt_save');
        a.attr('onclick', 'javascript:saveNotes("0")');        
        var b = document.getElementById('bt_del');
        b.attr('onclick', 'javascript:eLedgerupdatedeleteNotes("0")');
    } else {
        var a = document.getElementById('bt_save');
        a.click = "javascript:saveNotes('" + Id + "');";
        var b = document.getElementById('bt_del');
        b.click = "javascript:eLedgerupdatedeleteNotes('" + Id + "');";
        ValidateULN(uln, Id)
    }
}
function ValidateId(id) {
    var dt = new Date;
    var callurl = "/eledger2/api/Notes/validateuln/?id=" + id + "&date=" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    $.getJSON(callurl).done(function (data) {
        $.each(data, function (key, item) {
            $("#txt_NoteText").val(item.NoteText);
            tmpNote = item.NoteText
        })
    }).fail(function (jqXHR, textStatus, err) {
        return "false"
    })
}
function ValidateULN(ULN, id) {
    ValidateId(id)
}
function close_click() {
    window.close();
    return false
}
function saveNotes(id) {
    var uln = $('#hd_Uln').val();
    var mNote = document.getElementById("txt_NoteText").value;
    var url = "";
    if (validateForm(mNote)) {
        var flag = "false";
        if (id == 0) {
            flag = addeLedgerNotes(uln, mNote);
            window.close();
            if (flag == "false") {
                alert("There was a problem with the saving your note. Notify the Helpdesk.")
            }
        } else {
            flag = updatedeleteNotesdetails(uln, id, mNote);
            window.close()
        }
    }
    return false;
}
function validateForm(mNote) {
    var flag = true;
    if (mNote == "") {
        flag = false;
        alert("Must enter a note")
    }
    if (!isPageDirty() && flag) {
        flag = false;
        alert("No changes were made")
    }
    return flag
}
function isPageDirty() {
    var flag = false;
    if (tmpNote != document.getElementById("txt_NoteText").value)
        flag = true;
    return flag
}
function eLedgerupdatedeleteNotes(webid) {
    var uln = $('#hd_Uln').val();
    if (webid != "0") {
        deleteelegerNotes(uln, webid, "");
        window.close()
    } else {
        document.getElementById("txt_NoteText").value = ""
    }
}
function encodeNotestext(toEncode) {
    return encodeURIComponent(toEncode)
}
function updatedeleteNotesdetails(uln, id, notetext) {
    var callurl = "/eledger2/api/Notes/updatedeleteNotes/?uln=" + uln + "&id=" + id + "&notetext=" + encodeNotestext(notetext);
    $.ajax({
        url: callurl,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        type: "POST",
        async: false,
        success: function (e) {
            window.opener.refreshNotes();
        },
        error: function (result) {
            options.error(result)
        }
    });    
    return callurl
}
function deleteelegerNotes(uln, id, notetext) {
    tmpNote = "";
    var callurl = "/eledger2/api/Notes/deleteNotes/?uln=" + uln + "&id=" + id + "&notetext=" + encodeNotestext(notetext);
    $.ajax({
        url: callurl,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        type: "POST",
        async: false,
        success: function (e) {
            window.opener.refreshNotes();
        }
    });
}
function eLedgerAddNote(uln, noteText) {
    parent.addeLedgerNotes(uln, noteText);
    window.close()
}
function addeLedgerNotes(uln, noteText) {
    var callurl = "/eledger2/api/Notes/addNotesDetail?ULN=" + uln + "&noteText=" + encodeNotestext(noteText);
    $.ajax({
        url: callurl,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        type: "POST",
        async: false,
        success: function (e) {
            window.opener.refreshNotes();
        },
        error: function (result) {
            options.error(result)
        }
    });
    return callurl
}


