﻿using System.Web.Optimization;


namespace eLedgerMVC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            //the path of the stylebundle is important..as the CSS loads files from /files and /eledger2/default. 
            //this creates a virtual folder called "CSS" in /eledger2/
            bundles.Add(new StyleBundle("~/Content/eledger2/css").Include(
            "~/Content/eledger2/wash.css",
            "~/Content/eledger2/kendo.default.min.css",
            "~/Content/eledger2/bootstrap-responsive.css",
            "~/Content/eledger2/wash-responsive.css",
            "~/Content/eledger2/kendo.common.min.css",
            "~/Content/eledger2/normalize.css",
            "~/Content/eledger2/font-awesome.min.css"
             ));

            BundleTable.EnableOptimizations = true;
        }
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725

    }
}