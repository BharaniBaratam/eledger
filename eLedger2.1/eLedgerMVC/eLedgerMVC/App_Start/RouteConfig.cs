﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace eLedgerMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{uln}",
                defaults: new { controller = "Start", action = "search", uln = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "EqService",
               url: "{controller}/{action}/{uln}/{webid}",
              defaults: new { controller = "Start", action = "search" }
           );
            routes.MapRoute(
               name: "Service",
               url: "{controller}/{action}/{param1}/{param2}/{param3}/{param4}",
              defaults: new { controller = "Start", action = "search", param1 = UrlParameter.Optional, param2 = UrlParameter.Optional, param3 = UrlParameter.Optional, param4 = UrlParameter.Optional }
           );
            routes.MapRoute(
              name: "ExportExcel",
              url: "{controller}/{action}/{param1}/{param2}/{param3}",
             defaults: new { controller = "Start", action = "search", param1 = UrlParameter.Optional, param2 = UrlParameter.Optional, param3 = UrlParameter.Optional }
          );
            routes.MapRoute(
            name: "GoogleMapPlus",
            url: "{controller}/{action}/{uln}/{hostname}/{locationtype}/{distance}",
            defaults: new { controller = "Start", action = "search" }
        );

            routes.MapRoute(
           name: "LocationDetail",
           url: "{controller}/{action}/{ULN_id}/{ContractTypeName}",
           defaults: new { controller = "Search", action = "getLocationDetails", ContractTypeName = UrlParameter.Optional }
       );


            //public ActionResult getLocationDetails(string ULN_id, string ContractTypeName,Location loc)


            //     routes.MapRoute(
            //    name: "Alerts",
            //    url: "{controller}/{action}/{uln}/{showHistory}/{showFuture}",
            //    defaults: new { controller = "Start", action = "search" }
            //);
            //routes.MapRoute(
            //    name: "ServiceDetail",
            //    url: "{controller}/{action}/{uln}",
            //    defaults: new { controller = "Start", action = "search", service_call_id = UrlParameter.Optional }
            //);
            //routes.MapRoute(
            //    name: "AddNotes",
            //    url: "{controller}/{action}/{uln}/{id}",
            //    defaults: new { controller = "Start", action = "Index" }
            //);
            //routes.MapRoute(
            //    name: "LocationOverview",
            //    url: "{controller}/{action}/{uln}",
            //    defaults: new { controller = "Location", action = "getLocationOverview" ,uln=UrlParameter.Optional}
            //);
        }
    }
}