<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<script src="/eledger2/Scripts/eledger2/jquery-1.10.1.min.js"></script>
<% Context.Items["hostname"] = Request.Url.Host.ToLower();%>
<script>
    $(document).ready(function () {
        var host = "<%=Context.Items["hostname"]%>";
        var uln = "<%=Request["val"]%>";
        var querypanel = "<%=Request["querypanel"]%>";
        if (querypanel == "")
            querypanel = 1;
        var url = "";
        if (uln == null || uln.length == 0)
            uln = "<%=Request["ULN"]%>";
        var vport = (window.location.port != 80) ? ":" + window.location.port : "";
        if (uln == "") {
            url = "http://" + host + vport + "/eledger2/";
            window.open(url);
        }
        else {
            url = "http://" + host + vport + "/eledger2/#getLocDetail/" + uln+"/"+querypanel;
            //window.location.href = url;
            window.open(url, "_self");
        }
    });
</script>